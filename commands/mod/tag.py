import asyncio
import re
import discord
from discord.ext import commands
from library.utilities import UtilitiesLib
from library.user import UserLib
from library.messages import MessagesLib
from library.channel import ChannelLib
from storage.settings import SettingsLib
from storage.data import DataLib
from storage.tickets import TicketsLib

class TagCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.user_lib = UserLib()
        self.messages_lib = MessagesLib(bot)
        self.utilities_lib = UtilitiesLib(bot)
        self.channel_lib = ChannelLib(bot)
        self.settings_lib = SettingsLib()
        self.data_lib = DataLib()
        self.tickets_lib = TicketsLib(bot)

    @commands.group(name="tag", case_insensitive=True)
    async def command_tag(self, ctx, action, *, args=""):
        args = args.upper()

        message_object = ctx.message
        
        guild = message_object.guild
        channel = message_object.channel
        author = message_object.author

        action = str(action).upper()

        # Check if setup is done
        if not self.settings_lib.getSetupDone(guild):
            embed = self.messages_lib.setup_not_done(message_object)
            await ctx.send(embed=embed)
            return

        # No permissions
        if not self.user_lib.canExecute(author, "tag"):
            embed = self.messages_lib.no_permission(message_object)
            await ctx.send(embed=embed)
            return

        # Not a ticket channel
        if not self.channel_lib.isTicketChannel(channel):
            embed = self.messages_lib.not_ticket_channel(message_object)
            await ctx.send(embed=embed)
            return
        
        # List tags
        if action == "LIST":
            embed = self.messages_lib.tag_list_success(message_object)
            await ctx.send(embed=embed)
            return

        # Remove dangerous characters
        args = args.strip('\\')
        args = args.strip('\"')
        args = args.strip('\'')
        args = args.strip('*')
        args = args.strip('`')
        args = args.strip('_')
        args = args.strip('|')

        # Modify tags
        if self.tickets_lib.modTicketTags(channel, args, action): # Success
            if action == "ADD" or action == "APPEND":
                embed = self.messages_lib.tag_add_success(message_object)
                log_embed = self.messages_lib.logging_tag_add(message_object)
            if action == "REMOVE" or action == "DELETE":
                embed = self.messages_lib.tag_remove_success(message_object)
                log_embed = self.messages_lib.logging_tag_remove(message_object)

            await ctx.send(embed=embed)
            

            # Logging
            await self.utilities_lib.log(guild, log_embed)
            return
        
        raise commands.errors.BadArgument

    @command_tag.error
    async def tag_error(self, ctx, error):
        if isinstance(error, commands.errors.BadArgument) or isinstance(error, commands.errors.MissingRequiredArgument):
            embed = self.messages_lib.tag_fail(ctx.message)
            await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(TagCog(bot))