import asyncio
import discord
from discord.ext import commands
from library.utilities import UtilitiesLib
from library.user import UserLib
from library.messages import MessagesLib
from library.channel import ChannelLib
from library.checks import ChecksLib
from storage.settings import SettingsLib
from storage.tickets import TicketsLib

class RenameCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.user_lib = UserLib()
        self.messages_lib = MessagesLib(bot)
        self.utilities_lib = UtilitiesLib(bot)
        self.channel_lib = ChannelLib(bot)
        self.settings_lib = SettingsLib()
        self.tickets_lib = TicketsLib(bot)

    @commands.group(name="rename", case_insensitive=True)
    async def command_rename(self, ctx, *, args=""):
        message_object = ctx.message
        
        guild = message_object.guild
        channel = message_object.channel
        author = message_object.author

        # Check if setup is done
        if not self.settings_lib.getSetupDone(guild):
            embed = self.messages_lib.setup_not_done(message_object)
            await ctx.send(embed=embed)
            return

        # No permissions
        if not self.user_lib.canExecute(author, "rename"):
            embed = self.messages_lib.no_permission(message_object)
            await ctx.send(embed=embed)
            return

        # Not a ticket channel
        if not self.channel_lib.isTicketChannel(channel):
            embed = self.messages_lib.not_ticket_channel(message_object)
            await ctx.send(embed=embed)
            return

        # Rename the channel
        if args == "":
            raise commands.errors.BadArgument
        
        args = str(args[:100])
        await channel.edit(name=args, reason=self.messages_lib.modify_channel_name(message_object))

        # Modify ticket channel name in DB
        self.tickets_lib.setName(channel, channel.name)

        # Send success message
        embed = self.messages_lib.rename_success(message_object)
        await channel.send(embed=embed)
        
        
        # Logging
        log_embed = self.messages_lib.logging_rename(message_object)
        await self.utilities_lib.log(guild, log_embed)

    @command_rename.error
    async def rename_error(self, ctx, error):
        if isinstance(error, commands.errors.BadArgument):
            embed = self.messages_lib.rename_fail(ctx.message)
            await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(RenameCog(bot))