import asyncio
import typing
import discord
from discord.ext import commands
from library.utilities import UtilitiesLib
from library.user import UserLib
from library.messages import MessagesLib
from library.channel import ChannelLib
from library.checks import ChecksLib
from storage.settings import SettingsLib

class RemoveCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.user_lib = UserLib()
        self.messages_lib = MessagesLib(bot)
        self.utilities_lib = UtilitiesLib(bot)
        self.channel_lib = ChannelLib(bot)
        self.settings_lib = SettingsLib()

    @commands.group(name="remove", case_insensitive=True)
    async def command_remove(self, ctx, user: discord.Member, _channel: typing.Optional[discord.TextChannel]):
        message_object = ctx.message
        guild = message_object.guild
        channel = message_object.channel
        author = message_object.author

        # Check if setup is done
        if not self.settings_lib.getSetupDone(guild):
            embed = self.messages_lib.setup_not_done(message_object)
            await ctx.send(embed=embed)
            return

        # No permissions
        if not self.user_lib.canExecute(author, "remove"):
            embed = self.messages_lib.no_permission(message_object)
            await ctx.send(embed=embed)
            return

        # No channel argument specified
        if _channel == None:

            # Command not executed in a ticket channel
            if not self.channel_lib.isTicketChannel(channel):
                raise commands.errors.BadArgument

            # Command is executed in a ticket channel
            _channel = channel

        # Redefine permission overwrites (remove user from channel)
        await _channel.set_permissions(user, overwrite=None, reason=self.messages_lib.modify_channel_permissions(message_object))

        # Send success message
        embed = self.messages_lib.remove_success(message_object)
        await _channel.send(embed=embed)
        

        # Logging
        log_embed = self.messages_lib.logging_remove(message_object)
        await self.utilities_lib.log(guild, log_embed)

        # Check to avoid double-sending
        if not channel == _channel:
            await channel.send(embed=embed)
        
    @command_remove.error
    async def remove_error(self, ctx, error):
        if isinstance(error, commands.errors.BadArgument):
            embed = self.messages_lib.remove_fail(ctx.message)
            await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(RemoveCog(bot))