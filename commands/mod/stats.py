import discord
from discord.ext import commands
from library.user import UserLib
from library.utilities import UtilitiesLib
from library.messages import MessagesLib
from storage.settings import SettingsLib
from storage.tickets import TicketsLib

class StatsCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.user_lib = UserLib()
        self.utilities_lib = UtilitiesLib(bot)
        self.messages_lib = MessagesLib(bot)
        self.settings_lib = SettingsLib()
        self.tickets_lib = TicketsLib(bot)

    @commands.group(name="stats", case_insensitive=True)
    async def command_stats(self, ctx):
        message_object = ctx.message
        guild = message_object.guild
        author = message_object.author

        # Check if setup is done
        if not self.settings_lib.getSetupDone(guild):
            embed = self.messages_lib.setup_not_done(message_object)
            await ctx.send(embed=embed)
            return

        # No permissions
        if not self.user_lib.canExecute(author, "stats"):
            embed = self.messages_lib.no_permission(message_object)
            await ctx.send(embed=embed)
            return

        status = "BETA"
        total_tickets = self.tickets_lib.getGuildTicketCount(guild, open_only=False)
        open_tickets = self.tickets_lib.getGuildTicketCount(guild, open_only=True)
        top_close_1 = self.tickets_lib.getTopData(guild, "closed_by", 1)
        top_close_2 = self.tickets_lib.getTopData(guild, "closed_by", 2)
        top_close_3 = self.tickets_lib.getTopData(guild, "closed_by", 3)
        top_open_1 = self.tickets_lib.getTopData(guild, "created_by", 1)
        top_open_2 = self.tickets_lib.getTopData(guild, "created_by", 2)
        top_open_3 = self.tickets_lib.getTopData(guild, "created_by", 3)

        embed = discord.Embed(title=guild.name, description=self.bot.user.name + " - Ticket data", color=discord.Color.blue())
        embed.set_thumbnail(url=self.bot.user.avatar_url)
        embed.add_field(name="Bot Status", value=status, inline=True)
        embed.add_field(name="Tickets Created", value=total_tickets, inline=True)
        embed.add_field(name="Open Tickets", value=open_tickets, inline=True)
        embed.add_field(name="Most Resolved #1", value="**" + str(guild.get_member(top_close_1[0])) + "** \n*" + str(top_close_1[1]) + " Tickets*", inline=True)
        embed.add_field(name="Most Resolved #2", value="**" + str(guild.get_member(top_close_2[0])) + "** \n*" + str(top_close_2[1]) + " Tickets*", inline=True)
        embed.add_field(name="Most Resolved #3", value="**" + str(guild.get_member(top_close_3[0])) + "** \n*" + str(top_close_3[1]) + " Tickets*", inline=True)
        embed.add_field(name="Most Opened #1", value="**" + str(guild.get_member(top_open_1[0])) + "** \n*" + str(top_open_1[1]) + " Tickets*", inline=True)
        embed.add_field(name="Most Opened #2", value="**" + str(guild.get_member(top_open_2[0])) + "** \n*" + str(top_open_2[1]) + " Tickets*", inline=True)
        embed.add_field(name="Most Opened #3", value="**" + str(guild.get_member(top_open_3[0])) + "** \n*" + str(top_open_3[1]) + " Tickets*", inline=True)
        embed.set_footer(text="Support Tickets | By Zenya#0093")

        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(StatsCog(bot))