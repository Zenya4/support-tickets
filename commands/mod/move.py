import typing
import discord
from discord.ext import commands
from library.user import UserLib
from library.utilities import UtilitiesLib
from library.messages import MessagesLib
from library.channel import ChannelLib
from library.checks import ChecksLib
from storage.settings import SettingsLib

class MoveCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.user_lib = UserLib()
        self.utilities_lib = UtilitiesLib(bot)
        self.messages_lib = MessagesLib(bot)
        self.channel_lib = ChannelLib(bot)
        self.settings_lib = SettingsLib()

    @commands.group(name="move", case_insensitive=True)
    async def command_move(self, ctx, *, category_search: typing.Union[str, int] = None):
        message_object = ctx.message
        guild = message_object.guild
        channel = message_object.channel
        author = message_object.author

        # Check if setup is done
        if not self.settings_lib.getSetupDone(guild):
            embed = self.messages_lib.setup_not_done(message_object)
            await ctx.send(embed=embed)
            return

        # No permissions
        if not self.user_lib.canExecute(author, "move"):
            embed = self.messages_lib.no_permission(message_object)
            await ctx.send(embed=embed)
            return

        # No arguments
        if category_search == None:
            raise commands.errors.BadArgument

        # Check for category name
        category = self.channel_lib.getCategory(guild, str(category_search))
        if category == None: # Check for category ID
            category = guild.get_channel(int(category_search))

        if category == None: # Still not found
            raise commands.errors.BadArgument

        # Move channel
        await channel.edit(category=category, reason=self.messages_lib.move_channel(message_object))

        # Send success message
        embed = self.messages_lib.move_success(message_object)
        await channel.send(embed=embed)
        
        # Logging
        log_embed = self.messages_lib.logging_move(message_object)
        await self.utilities_lib.log(guild, log_embed)
    
    @command_move.error
    async def add_error(self, ctx, error):
        if isinstance(error, commands.errors.BadUnionArgument) or isinstance(error, commands.errors.BadArgument):
            embed = self.messages_lib.move_fail(ctx.message)
            await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(MoveCog(bot))