import asyncio
import typing
import discord
from discord.ext import commands
from library.utilities import UtilitiesLib
from library.user import UserLib
from library.messages import MessagesLib
from library.channel import ChannelLib
from library.checks import ChecksLib
from storage.settings import SettingsLib

class LockCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.user_lib = UserLib()
        self.messages_lib = MessagesLib(bot)
        self.utilities_lib = UtilitiesLib(bot)
        self.channel_lib = ChannelLib(bot)
        self.settings_lib = SettingsLib()

    @commands.group(name="lock", case_insensitive=True)
    async def command_lock(self, ctx):
        message_object = ctx.message
        
        guild = message_object.guild
        channel = message_object.channel
        author = message_object.author

        # Check if setup is done
        if not self.settings_lib.getSetupDone(guild):
            embed = self.messages_lib.setup_not_done(message_object)
            await ctx.send(embed=embed)
            return

        # No permissions
        if not self.user_lib.canExecute(author, "lock"):
            embed = self.messages_lib.no_permission(message_object)
            await ctx.send(embed=embed)
            return

        # Command not executed in a ticket channel
        if not self.channel_lib.isTicketChannel(channel):
            embed = self.messages_lib.not_ticket_channel(message_object)
            await ctx.send(embed=embed)
            return

        # Define overwrites
        readwrite_perm = discord.PermissionOverwrite(read_messages=True, send_messages=True)
        readonly_perm = discord.PermissionOverwrite(read_messages=True, send_messages=False)
        no_perm = discord.PermissionOverwrite(read_messages=False, send_messages=False)

        # Define roles and members
        readwrite_group = []
        readonly_group = []
        noperm_group = []

        for item in channel.overwrites: 

            if isinstance(item, discord.Role):
            # Admin & Mod roles have readwrite
            # Other roles have no perms
                if self.utilities_lib.isPrivilegedRole(guild, item, "ADMIN") or self.utilities_lib.isPrivilegedRole(guild, item, "MOD"):
                    readwrite_group.append(item)
                else:
                    noperm_group.append(item)

            elif isinstance(item, discord.Member):
            # Bot (self) has readwrite
            # Other members have readonly
                if item.id == guild.me.id:
                    readwrite_group.append(item)
                else:
                    readonly_group.append(item)

        # Actually set channel permissions
        for group in readwrite_group:
            await channel.set_permissions(group, overwrite=readwrite_perm, reason=self.messages_lib.modify_channel_permissions(message_object))
        for group in readonly_group:
            await channel.set_permissions(group, overwrite=readonly_perm, reason=self.messages_lib.modify_channel_permissions(message_object))
        for group in noperm_group:
            await channel.set_permissions(group, overwrite=no_perm, reason=self.messages_lib.modify_channel_permissions(message_object))
            
        # Send success message
        embed = self.messages_lib.lock_success(message_object)
        await channel.send(embed=embed)
        

        # Logging
        log_embed = self.messages_lib.logging_lock(message_object)
        await self.utilities_lib.log(guild, log_embed)

def setup(bot):
    bot.add_cog(LockCog(bot))