import asyncio
import typing
import discord
from discord.ext import commands
from library.utilities import UtilitiesLib
from library.user import UserLib
from library.messages import MessagesLib
from library.channel import ChannelLib
from library.checks import ChecksLib
from storage.settings import SettingsLib

class UnlockCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.user_lib = UserLib()
        self.messages_lib = MessagesLib(bot)
        self.utilities_lib = UtilitiesLib(bot)
        self.channel_lib = ChannelLib(bot)
        self.settings_lib = SettingsLib()

    @commands.group(name="unlock", case_insensitive=True)
    async def command_unlock(self, ctx):
        message_object = ctx.message
        
        guild = message_object.guild
        channel = message_object.channel
        author = message_object.author

        # Check if setup is done
        if not self.settings_lib.getSetupDone(guild):
            embed = self.messages_lib.setup_not_done(message_object)
            await ctx.send(embed=embed)
            return

        # No permissions
        if not self.user_lib.canExecute(author, "unlock"):
            embed = self.messages_lib.no_permission(message_object)
            await ctx.send(embed=embed)
            return

        # Command not executed in a ticket channel
        if not self.channel_lib.isTicketChannel(channel):
            embed = self.messages_lib.not_ticket_channel(message_object)
            await ctx.send(embed=embed)
            return

        # Define overwrites
        readwrite_perm = discord.PermissionOverwrite(read_messages=True, send_messages=True)
        readonly_perm = discord.PermissionOverwrite(read_messages=True, send_messages=False)

        # Replace readonly with readwrite
        for item in channel.overwrites:
            if channel.overwrites[item] == readonly_perm:
                await channel.set_permissions(item, overwrite=readwrite_perm, reason=self.messages_lib.modify_channel_permissions(message_object))
            
        # Send success message
        embed = self.messages_lib.unlock_success(message_object)
        await channel.send(embed=embed)
        

        # Logging
        log_embed = self.messages_lib.logging_unlock(message_object)
        await self.utilities_lib.log(guild, log_embed)

def setup(bot):
    bot.add_cog(UnlockCog(bot))