import typing
import discord
from discord.ext import commands
from library.utilities import UtilitiesLib
from library.user import UserLib
from library.messages import MessagesLib
from library.channel import ChannelLib
from library.checks import ChecksLib
from storage.settings import SettingsLib

class AddCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.user_lib = UserLib()
        self.messages_lib = MessagesLib(bot)
        self.utilities_lib = UtilitiesLib(bot)
        self.channel_lib = ChannelLib(bot)
        self.settings_lib = SettingsLib()

    @commands.group(name="add", case_insensitive=True)
    async def command_add(self, ctx, user: discord.Member, _channel: typing.Optional[discord.TextChannel]):
        message_object = ctx.message
        guild = message_object.guild
        channel = message_object.channel
        author = message_object.author

        # Check if setup is done
        if not self.settings_lib.getSetupDone(guild):
            embed = self.messages_lib.setup_not_done(message_object)
            await ctx.send(embed=embed)
            return

        # No permissions
        if not self.user_lib.canExecute(author, "add"):
            embed = self.messages_lib.no_permission(message_object)
            await ctx.send(embed=embed)
            return

        # No channel argument specified
        if _channel == None:

            # Command not executed in a ticket channel
            if not self.channel_lib.isTicketChannel(channel):
                raise commands.errors.BadArgument

            _channel = channel
        
        # Channel argument specified
        else:

            # Channel argument is not a ticket channel
            if not self.channel_lib.isTicketChannel(_channel):
                raise commands.errors.BadArgument

        # Redefine permission overwrites (add user to channel)
        overwrite = discord.PermissionOverwrite(read_messages=True, send_messages=True)
        await _channel.set_permissions(user, overwrite=overwrite, reason=self.messages_lib.modify_channel_permissions(message_object))

        # Send success message
        embed = self.messages_lib.add_success(message_object)
        await _channel.send(embed=embed)
        

        # Logging
        log_embed = self.messages_lib.logging_add(message_object)
        await self.utilities_lib.log(guild, log_embed)

        # Check to avoid double-sending
        if not channel == _channel:
            await channel.send(embed=embed)
    
    @command_add.error
    async def add_error(self, ctx, error):
        if isinstance(error, commands.errors.BadArgument):
            embed = self.messages_lib.add_fail(ctx.message)
            await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(AddCog(bot))