import asyncio
import discord
from discord.ext import commands
from library.utilities import UtilitiesLib
from library.user import UserLib
from library.messages import MessagesLib
from storage.settings import SettingsLib
from storage.data import DataLib

class BlacklistCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.user_lib = UserLib()
        self.messages_lib = MessagesLib(bot)
        self.utilities_lib = UtilitiesLib(bot)
        self.settings_lib = SettingsLib()
        self.data_lib = DataLib()

    @commands.group(name="blacklist", case_insensitive=True)
    async def command_blacklist(self, ctx, action, args: discord.Member = None):
        message_object = ctx.message
        
        guild = message_object.guild
        author = message_object.author

        action = str(action).upper()

        # Check if setup is done
        if not self.settings_lib.getSetupDone(guild):
            embed = self.messages_lib.setup_not_done(message_object)
            await ctx.send(embed=embed)
            return

        # No permissions
        if not self.user_lib.canExecute(author, "blacklist"):
            embed = self.messages_lib.no_permission(message_object)
            await ctx.send(embed=embed)
            return
        
        # List blacklist
        if action == "LIST":
            embed = self.messages_lib.blacklist_list_success(message_object)
            await ctx.send(embed=embed)
            return

        # Modify blacklist
        if self.data_lib.modBlacklistedMember(guild, args, action): # Success
            if action == "ADD" or action == "APPEND":
                embed = self.messages_lib.blacklist_add_success(message_object)
                log_embed = self.messages_lib.logging_blacklist_add(message_object)
            if action == "REMOVE" or action == "DELETE":
                embed = self.messages_lib.blacklist_remove_success(message_object)
                log_embed = self.messages_lib.logging_blacklist_remove(message_object)

            await ctx.send(embed=embed)
            

            # Logging
            await self.utilities_lib.log(guild, log_embed)
            return
        
        raise commands.errors.BadArgument
    
    @command_blacklist.error
    async def blacklist_error(self, ctx, error):
        if isinstance(error, commands.errors.BadArgument) or isinstance(error, commands.errors.MissingRequiredArgument):
            embed = self.messages_lib.blacklist_fail(ctx.message)
            await ctx.send(embed=embed)
        
def setup(bot):
    bot.add_cog(BlacklistCog(bot))