import discord
from discord.ext import commands
from library.user import UserLib
from library.utilities import UtilitiesLib
from library.messages import MessagesLib
from storage.settings import SettingsLib
from storage.data import DataLib

class PrefixCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.user_lib = UserLib()
        self.utilities_lib = UtilitiesLib(bot)
        self.messages_lib = MessagesLib(bot)
        self.settings_lib = SettingsLib()
        self.data_lib = DataLib()

    @commands.group(name="prefix", case_insensitive=True)
    async def command_prefix(self, ctx, *, args=""):

        # Define variables
        msg_object = ctx.message
        guild = msg_object.guild
        author = msg_object.author

        # Check if setup is done
        if not self.settings_lib.getSetupDone(guild):
            embed = self.messages_lib.setup_not_done(msg_object)
            await ctx.send(embed=embed)
            return

        # Check for permissions
        if not self.user_lib.canExecute(author, "prefix"):
            embed = self.messages_lib.no_permission(msg_object)
            await ctx.send(embed=embed)
            return

        # No arguments
        if args == "":
            embed = self.messages_lib.prefix_set_fail(msg_object)
            await ctx.send(embed=embed)
            return

        # Ensure mysql parses safely
        safe_sql = "\\" + "\\"
        args = args.replace("\\", safe_sql)
        safe_sql = "\\" + "'"
        args = args.replace("'", safe_sql)

        self.data_lib.setGuildPrefix(guild, args)
        embed = self.messages_lib.prefix_set_success(msg_object)
        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(PrefixCog(bot))
