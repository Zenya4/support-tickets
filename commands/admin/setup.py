import discord
from discord.ext import commands
from library.utilities import UtilitiesLib
from library.user import UserLib
from library.messages import MessagesLib
from library.channel import ChannelLib
from library.checks import ChecksLib
from storage.settings import SettingsLib
from storage.data import DataLib
from storage.tickets import TicketsLib

class SetupCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.user_lib = UserLib()
        self.messages_lib = MessagesLib(bot)
        self.utilities_lib = UtilitiesLib(bot)
        self.channel_lib = ChannelLib(bot)
        self.settings_lib = SettingsLib()
        self.data_lib = DataLib()
        self.tickets_lib = TicketsLib(bot)

    def initBot(self):
        self.settings_lib.initBotSettings()
        self.data_lib.initBotData()
        self.tickets_lib.initBotTickets()

    @commands.group(name='setup', case_insensitive=True)
    async def command_setup(self, ctx, *, args=""):
        args = args.upper()
        
        # Define variables
        msg_call = ctx.message
        guild = msg_call.guild
        channel = msg_call.channel
        author = msg_call.author

        checks_lib = ChecksLib(author, channel)

        # Define content-setting methods
        # Set Administrator roles
        async def setAdminRoles():
            admin_roles = []
            admin_role_mentions = []

            embed = self.messages_lib.admin_roles_ask(msg_call)
            await ctx.send(embed=embed)

            msg_resp = await self.bot.wait_for('message', check=checks_lib.check_global)

            # Check for request cancellation
            if not checks_lib.check_cancel(msg_resp):
                embed = self.messages_lib.setup_cancelled(msg_resp)
                await ctx.send(embed=embed)
                return False

            # Check for roles
            if not checks_lib.check_roles(msg_resp):
                embed = self.messages_lib.admin_roles_set_fail(msg_resp)
                await ctx.send(embed=embed)
                return await setAdminRoles()

            for role in msg_resp.role_mentions:
                admin_roles.append(role)
                admin_role_mentions.append(role.mention)

            self.settings_lib.setPrivilegedRoles(guild, "ADMIN", admin_roles)
            embed = self.messages_lib.admin_roles_set_success(msg_resp)
            await ctx.send(embed=embed)
            return True

        # Set Moderator roles
        async def setModRoles():
            mod_roles = []
            mod_role_mentions = []

            embed = self.messages_lib.mod_roles_ask(msg_call)
            await ctx.send(embed=embed)

            msg_resp = await self.bot.wait_for('message', check=checks_lib.check_global)

            # Check for request cancellation
            if not checks_lib.check_cancel(msg_resp):
                embed = self.messages_lib.setup_cancelled(msg_resp)
                await ctx.send(embed=embed)
                return False

            # Check for request skip
            if not checks_lib.check_skip(msg_resp):
                embed = self.messages_lib.setup_setting_skipped(msg_resp)
                await ctx.send(embed=embed)
                return True

            # Check for roles
            if not checks_lib.check_roles(msg_resp):
                embed = self.messages_lib.mod_roles_set_fail(msg_resp)
                await ctx.send(embed=embed)
                return await setModRoles()

            for role in msg_resp.role_mentions:
                mod_roles.append(role)
                mod_role_mentions.append(role.mention)

            self.settings_lib.setPrivilegedRoles(guild, "MOD", mod_roles)
            embed = self.messages_lib.mod_roles_set_success(msg_resp)
            await ctx.send(embed=embed)
            return True

        # Set Mentioned roles
        async def setMentionRoles():
            mention_roles = []
            mention_role_mentions = []

            embed = self.messages_lib.mention_roles_ask(msg_call)
            await ctx.send(embed=embed)

            msg_resp = await self.bot.wait_for('message', check=checks_lib.check_global)

            # Check for request cancellation
            if not checks_lib.check_cancel(msg_resp):
                embed = self.messages_lib.setup_cancelled(msg_resp)
                await ctx.send(embed=embed)
                return False
            
            # Check for request skip
            if not checks_lib.check_skip(msg_resp):
                embed = self.messages_lib.setup_setting_skipped(msg_resp)
                await ctx.send(embed=embed)
                return True

            # Check for roles
            if not checks_lib.check_roles(msg_resp):
                embed = self.messages_lib.mention_roles_set_fail(msg_resp)
                await ctx.send(embed=embed)
                return await setMentionRoles()

            for role in msg_resp.role_mentions:
                mention_roles.append(role)
                mention_role_mentions.append(role.mention)

            self.settings_lib.setPrivilegedRoles(guild, "MENTION", mention_roles)
            embed = self.messages_lib.mention_roles_set_success(msg_resp)
            await ctx.send(embed=embed)
            return True

        # Set ticket-creation cooldown
        async def setTicketCooldown():
            embed = self.messages_lib.ticket_cooldown_ask(msg_call)
            await ctx.send(embed=embed)

            msg_resp = await self.bot.wait_for('message', check=checks_lib.check_global)

            # Check for request cancellation
            if not checks_lib.check_cancel(msg_resp):
                embed = self.messages_lib.setup_cancelled(msg_resp)
                await ctx.send(embed=embed)
                return False

            # Check for request skip
            if not checks_lib.check_skip(msg_resp):
                embed = self.messages_lib.setup_setting_skipped(msg_resp)
                await ctx.send(embed=embed)
                return True

            # Check for integer
            if not checks_lib.check_int(msg_resp):
                embed = self.messages_lib.ticket_cooldown_set_fail(msg_resp)
                await ctx.send(embed=embed)
                return await setTicketCooldown()

            ticket_cooldown = int(msg_resp.content)
            self.settings_lib.setTicketCooldown(guild, ticket_cooldown)
            embed = self.messages_lib.ticket_cooldown_set_success(msg_resp)
            await ctx.send(embed=embed)
            return True

        # Set ticket-creation category
        async def setTicketCategory():
            embed = self.messages_lib.ticket_category_ask(msg_call)
            await ctx.send(embed=embed)

            msg_resp = await self.bot.wait_for('message', check=checks_lib.check_global)

            # Check for request cancellation
            if not checks_lib.check_cancel(msg_resp):
                embed = self.messages_lib.setup_cancelled(msg_resp)
                await ctx.send(embed=embed)
                return False

            # Check for request skip
            if not checks_lib.check_skip(msg_resp):
                embed = self.messages_lib.setup_setting_skipped(msg_resp)
                await ctx.send(embed=embed)
                return True

            # Check for category name
            ticket_category = self.channel_lib.getCategory(guild, msg_resp.content)
            if ticket_category is None:
                # Check for category ID
                if not checks_lib.check_category(msg_resp):
                    embed = self.messages_lib.ticket_category_set_fail(msg_resp)
                    await ctx.send(embed=embed)
                    return await setTicketCategory()

                ticket_category_id = int(msg_resp.content)
                ticket_category = guild.get_channel(ticket_category_id)

            self.settings_lib.setTicketCategory(guild, ticket_category)
            embed = self.messages_lib.ticket_category_set_success(msg_resp)
            await ctx.send(embed=embed)
            return True

        # Set ticket-creation limit
        async def setTicketMax():
            embed = self.messages_lib.ticket_max_ask(msg_call)
            await ctx.send(embed=embed)

            msg_resp = await self.bot.wait_for('message', check=checks_lib.check_global)

            # Check for request cancellation
            if not checks_lib.check_cancel(msg_resp):
                embed = self.messages_lib.setup_cancelled(msg_resp)
                await ctx.send(embed=embed)
                return False

            # Check for request skip
            if not checks_lib.check_skip(msg_resp):
                embed = self.messages_lib.setup_setting_skipped(msg_resp)
                await ctx.send(embed=embed)
                return True

            # Check for integer
            if not checks_lib.check_int(msg_resp):
                embed = self.messages_lib.ticket_max_set_fail(msg_resp)
                await ctx.send(embed=embed)
                return await setTicketMax()

            ticket_max = int(msg_resp.content)
            self.settings_lib.setTicketMax(guild, ticket_max)
            embed = self.messages_lib.ticket_max_set_success(msg_resp)
            await ctx.send(embed=embed)
            return True

        # Set reaction time before ticket is actually closed after -close command
        async def setTicketCloseTime():
            embed = self.messages_lib.ticket_close_time_ask(msg_call)
            await ctx.send(embed=embed)

            msg_resp = await self.bot.wait_for('message', check=checks_lib.check_global)

            # Check for request cancellation
            if not checks_lib.check_cancel(msg_resp):
                embed = self.messages_lib.setup_cancelled(msg_resp)
                await ctx.send(embed=embed)
                return False

            # Check for request skip
            if not checks_lib.check_skip(msg_resp):
                embed = self.messages_lib.setup_setting_skipped(msg_resp)
                await ctx.send(embed=embed)
                return True

            # Check for integer
            if not checks_lib.check_int(msg_resp):
                embed = self.messages_lib.ticket_close_time_set_fail(msg_resp)
                await ctx.send(embed=embed)
                return await setTicketCloseTime()

            close_time = int(msg_resp.content)
            self.settings_lib.setTicketCloseTime(guild, close_time)
            embed = self.messages_lib.ticket_close_time_set_success(msg_resp)
            await ctx.send(embed=embed)
            return True

        # Set logging channel
        async def setLoggingChannel():
            embed = self.messages_lib.logging_channel_ask(msg_call)
            await ctx.send(embed=embed)

            msg_resp = await self.bot.wait_for('message', check=checks_lib.check_global)

            # Check for request cancellation
            if not checks_lib.check_cancel(msg_resp):
                embed = self.messages_lib.setup_cancelled(msg_resp)
                await ctx.send(embed=embed)
                return False

            # Check for request skip
            if not checks_lib.check_skip(msg_resp):
                embed = self.messages_lib.setup_setting_skipped(msg_resp)
                await ctx.send(embed=embed)
                return True

            # Check for channel mention
            try:
                logging_channel = msg_resp.channel_mentions[0]
            except:
                embed = self.messages_lib.logging_channel_set_fail(msg_resp)
                await ctx.send(embed=embed)
                return await setLoggingChannel()

            self.settings_lib.setLoggingChannel(guild, logging_channel)
            embed = self.messages_lib.logging_channel_set_success(msg_resp)
            await ctx.send(embed=embed)
            return True
        
        # Set of the above, and initialise
        async def initialiseGuild():
            if await setAdminRoles():
                if await setModRoles():
                    if await setMentionRoles():
                        if await setTicketCooldown():
                            if await setTicketCategory():
                                if await setTicketMax():
                                    if await setTicketCloseTime():
                                        if await setLoggingChannel():
                                            self.settings_lib.setSetupDone(guild)

                                            embed = self.messages_lib.setup_done(msg_call)
                                            await ctx.send(embed=embed)

        # Check for permissions
        if not self.user_lib.canExecute(author, "setup"):
            embed = self.messages_lib.no_permission(msg_call)
            await ctx.send(embed=embed)
            return

        # Begin switching args
        if args == "ADMIN-ROLE" or args == "ADMIN-ROLES":
            await setAdminRoles()
        elif args == "MOD-ROLE" or args == "MOD-ROLES":
            await setModRoles()
        elif args == "MENTION-ROLE" or args == "MENTION-ROLES":
            await setMentionRoles()
        elif args == "TICKET-COOLDOWN":
            await setTicketCooldown()
        elif args == "TICKET-CATEGORY":
            await setTicketCategory()
        elif args == "TICKET-MAX":
            await setTicketMax()
        elif args == "TICKET-CLOSE-TIME":
            await setTicketCloseTime()
        elif args == "LOGGING-CHANNEL":
            await setLoggingChannel()
        elif args == "":
            await initialiseGuild()
        else:
            return
            # TODO: Send help message

def setup(bot):
    bot.add_cog(SetupCog(bot))