import threading
import time
from datetime import datetime
import discord
from discord.ext import commands
from library.utilities import UtilitiesLib
from library.user import UserLib
from library.messages import MessagesLib
from library.channel import ChannelLib
from library.cooldowns import CooldownsLib
from storage.settings import SettingsLib
from storage.data import DataLib
from storage.tickets import TicketsLib

class NewCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.user_lib = UserLib()
        self.messages_lib = MessagesLib(bot)
        self.utilities_lib = UtilitiesLib(bot)
        self.channel_lib = ChannelLib(bot)
        self.cooldowns_lib = CooldownsLib()
        self.settings_lib = SettingsLib()
        self.data_lib = DataLib()
        self.tickets_lib = TicketsLib(bot)

    # Cooldown manager
    def doCooldown(self, guild, author):
        while True:
            time.sleep(1)     
            time_left = self.cooldowns_lib.getCooldown(guild, author)

            if time_left is None:
                return

            self.cooldowns_lib.setCooldown(guild, author, time_left-1)

    @commands.group(name="new", aliases=['ticket'], case_insensitive=True)
    async def command_new(self, ctx, *, args="Support Request"):
        message_object = ctx.message
        
        guild = message_object.guild
        author = message_object.author

        ticket_mentions = []

        # Check if setup is done
        if not self.settings_lib.getSetupDone(guild):
            embed = self.messages_lib.setup_not_done(message_object)
            await ctx.send(embed=embed)
            return

        # No permissions
        if not self.user_lib.canExecute(author, "new"):
            embed = self.messages_lib.no_permission(message_object)
            await ctx.send(embed=embed)
            return

        # User in blacklist + rank check
        blacklist = self.data_lib.getBlacklistedMembers(guild)
        if author in blacklist and self.user_lib.getRank(author) == "MEMBER":
            embed = self.messages_lib.user_in_blacklist(message_object)
            await ctx.send(embed=embed)
            return

        # User has hit max tickets + rank check
        user_tickets = self.tickets_lib.getUserTicketCount(guild, author)
        ticket_count = self.settings_lib.getTicketMax(guild)

        if user_tickets >= ticket_count and self.user_lib.getRank(author) == "MEMBER":
            embed = self.messages_lib.ticket_max_reached(message_object)
            await ctx.send(embed=embed)
            return

        guild_cooldown = self.settings_lib.getTicketCooldown(guild)
        time_left = self.cooldowns_lib.getCooldown(guild, author)

        # Under cooldown + rank check
        if time_left is not None and self.user_lib.getRank(author) == "MEMBER":
            embed = self.messages_lib.command_under_cooldown(message_object)
            await ctx.send(embed=embed)
            return

        # Add to cooldown
        self.cooldowns_lib.setCooldown(guild, author, guild_cooldown)

        cooldown_thread = threading.Thread(target=self.doCooldown, args=(guild, author))
        cooldown_thread.start()

        # Create ticket channel
        ticket_channel = await self.channel_lib.createTicketChannel(message_object)
        self.tickets_lib.createTicket(ticket_channel, ticket_channel.name, ticket_channel.guild, author, datetime.today().strftime('%Y-%m-%d'))

        # Ping relevant roles and users in ticket
        mention_roles = self.settings_lib.getPrivilegedRoles(guild, "MENTION")
        for role in mention_roles:
            ticket_mentions.append(role.mention)
        ticket_mentions.append(author.mention)
        await ticket_channel.send(content=' '.join(ticket_mentions), delete_after=0)

        # Send welcome message in ticket
        embed = self.messages_lib.new_title(message_object)
        await ticket_channel.send(embed=embed)

        # Send success message
        embed = self.messages_lib.new_success(message_object)
        await ctx.send(embed=embed)
        

        # Logging
        log_embed = self.messages_lib.logging_new(message_object)
        await self.utilities_lib.log(guild, log_embed)

def setup(bot):
    bot.add_cog(NewCog(bot))