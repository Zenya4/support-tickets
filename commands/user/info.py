import discord
from discord.ext import commands
from library.user import UserLib
from library.utilities import UtilitiesLib
from library.messages import MessagesLib
from storage.settings import SettingsLib

class InfoCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.user_lib = UserLib()
        self.utilities_lib = UtilitiesLib(bot)
        self.messages_lib = MessagesLib(bot)
        self.settings_lib = SettingsLib()

    @commands.group(name="info", aliases=['invite'], case_insensitive=True)
    async def command_info(self, ctx):
        message_object = ctx.message
        author = message_object.author

        # No permissions
        if not self.user_lib.canExecute(author, "info"):
            embed = self.messages_lib.no_permission(message_object)
            await ctx.send(embed=embed)
            return

        embed = discord.Embed(title=self.bot.user.name, description="A feature-rich support ticket bot for Discord", color=discord.Color.blue())
        embed.set_thumbnail(url=self.bot.user.avatar_url)
        embed.add_field(name="Creator", value="Zenya#0093", inline=True)
        embed.add_field(name="Servers", value=len(self.bot.guilds), inline=True)
        embed.add_field(name="Users", value=len(self.bot.users), inline=True)
        embed.add_field(name="Invite", value="[Website](https://discordbot.zenya.dev/)", inline=True)
        embed.add_field(name="Support", value="[Discord](https://discord.gg/KGuaxpM)", inline=True)
        embed.add_field(name="Donate", value="[PayPal](https://paypal.me/zenya4)", inline=True)
        embed.set_footer(text="Support Tickets | By Zenya#0093")

        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(InfoCog(bot))