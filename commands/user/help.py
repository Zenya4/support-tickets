import discord
from discord.ext import commands
from library.user import UserLib
from library.messages import MessagesLib
from storage.settings import SettingsLib

class HelpCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.user_lib = UserLib()
        self.messages_lib = MessagesLib(bot)
        self.settings_lib = SettingsLib()

        self.bot.remove_command("help") # Remove default help command

    @commands.group(name="help", case_insensitive=True)
    async def command_help(self, ctx, args=""):
        message_object = ctx.message
        guild = message_object.guild
        author = message_object.author

        # Check if setup is done
        if not self.settings_lib.getSetupDone(guild):
            embed = self.messages_lib.setup_not_done(message_object)
            await ctx.send(embed=embed)
            return

        # No permissions
        if not self.user_lib.canExecute(author, "help"):
            embed = self.messages_lib.no_permission(message_object)
            await ctx.send(embed=embed)
            return

        # Switch args
        switcher = {
            "HELP": self.messages_lib.help(message_object, "help"),
            "NEW": self.messages_lib.help(message_object, "new"),
            "CLOSE": self.messages_lib.help(message_object, "close"),
            "INFO": self.messages_lib.help(message_object, "info"),
            "ADD": self.messages_lib.help(message_object, "add"),
            "BLACKLIST": self.messages_lib.help(message_object, "blacklist"),
            "LOCK": self.messages_lib.help(message_object, "lock"),
            "MOVE": self.messages_lib.help(message_object, "move"),
            "REMOVE": self.messages_lib.help(message_object, "remove"),
            "RENAME": self.messages_lib.help(message_object, "rename"),
            "STATS": self.messages_lib.help(message_object, "stats"),
            "TAG": self.messages_lib.help(message_object, "tag"),
            "TRANSCRIPT": self.messages_lib.help(message_object, "transcript"),
            "UNLOCK": self.messages_lib.help(message_object, "unlock"),
            "PREFIX": self.messages_lib.help(message_object, "prefix"),
            "SEARCH": self.messages_lib.help(message_object, "search"),
            "SETUP": self.messages_lib.help(message_object, "setup")
        }

        embed = switcher.get(args.upper(), self.messages_lib.help(message_object, "help"))
        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(HelpCog(bot))