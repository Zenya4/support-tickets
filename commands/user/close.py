import asyncio
from datetime import datetime
import discord
from discord.ext import commands
from library.utilities import UtilitiesLib
from library.user import UserLib
from library.messages import MessagesLib
from library.channel import ChannelLib
from library.checks import ChecksLib
from storage.settings import SettingsLib
from storage.tickets import TicketsLib

class CloseCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.user_lib = UserLib()
        self.messages_lib = MessagesLib(bot)
        self.utilities_lib = UtilitiesLib(bot)
        self.channel_lib = ChannelLib(bot)
        self.settings_lib = SettingsLib()
        self.tickets_lib = TicketsLib(bot)

    @commands.group(name="close", case_insensitive=True)
    async def command_close(self, ctx, *, args="Issue resolved"):
        message_object = ctx.message
        guild = message_object.guild
        channel = message_object.channel
        author = message_object.author

        checks_lib = ChecksLib(author, channel)

        # Check if setup is done
        if not self.settings_lib.getSetupDone(guild):
            embed = self.messages_lib.setup_not_done(message_object)
            await ctx.send(embed=embed)
            return

        # No permissions
        if not self.user_lib.canExecute(author, "close"):
            embed = self.messages_lib.no_permission(message_object)
            await ctx.send(embed=embed)
            return

        # Not a ticket channel
        if not self.channel_lib.isTicketChannel(channel):
            embed = self.messages_lib.not_ticket_channel(message_object)
            await ctx.send(embed=embed)
            return

        # Get close timeout
        timeout = self.settings_lib.getTicketCloseTime(guild)

        # Wait for x seconds before closing
        embed = self.messages_lib.close_warning(message_object)
        await ctx.send(embed=embed)

        try: # Confirm closing of ticket
            await self.bot.wait_for('message', check=checks_lib.check_channel, timeout=timeout)
        except asyncio.TimeoutError: # Close ticket
            # Logging
            log_embed = self.messages_lib.logging_close(message_object)
            await self.utilities_lib.log(guild, log_embed)

            await channel.delete(reason=self.messages_lib.delete_channel(message_object))
            self.tickets_lib.setTicketOpen(channel, False)
            self.tickets_lib.setResolver(channel, author)
            self.tickets_lib.setResolutionDate(channel, datetime.today().strftime('%Y-%m-%d'))
        else: # Abort action
            embed = self.messages_lib.close_abort(message_object)
            await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(CloseCog(bot))