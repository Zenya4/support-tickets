import discord
from discord.ext import commands
import os
import datetime
import sys
import traceback
from storage.settings import SettingsLib
from storage.data import DataLib

settings_lib = SettingsLib()
data_lib = DataLib()

token_file = data_lib.token_file

intents = discord.Intents.default()
intents.members = True

with open(token_file, 'r') as f:
    TOKEN = f.readlines()[0]
    f.close()

def get_prefix(bot, message):
    prefixes = ['-']

    if not message.guild:
        return commands.when_mentioned_or(*prefixes)(bot, message)

    guild = message.guild

    if not data_lib.getGuildExists(guild):
        pass
    else:
        custom_prefix = data_lib.getPrefix(guild)
        prefixes.append(custom_prefix)

    return commands.when_mentioned_or(*prefixes)(bot, message)

bot = commands.Bot(command_prefix=get_prefix, intents=intents, fetch_offline_members=True)

initial_extensions = []

admin_extensions = [
    'commands.admin.prefix',  
#    'commands.admin.search',  
    'commands.admin.setup']
initial_extensions.extend(admin_extensions)

mod_extensions = [
    'commands.mod.add', 
    'commands.mod.blacklist', 
    'commands.mod.lock', 
    'commands.mod.move', 
    'commands.mod.remove', 
    'commands.mod.rename', 
    'commands.mod.stats', 
    'commands.mod.tag', 
#    'commands.mod.transcript', 
    'commands.mod.unlock']
initial_extensions.extend(mod_extensions)

user_extensions = [
    'commands.user.close', 
    'commands.user.help', 
    'commands.user.info', 
    'commands.user.new']
initial_extensions.extend(user_extensions)

if __name__ == '__main__':
    for extension in initial_extensions:
        try:
            bot.load_extension(extension)
        except Exception as e:
            print(f'Failed to load extension {extension}')
            traceback.print_exc()

@bot.event
async def on_ready():
    game = discord.Activity(type=discord.ActivityType.watching, name='over Tickets')
    await bot.change_presence(activity=game)
    print(f'Logged in as {bot.user.name} with ID {bot.user.id}')
    print(f'Version: {discord.__version__}')

# Ignore DMs
@bot.event
async def on_message(message):
    if not message.guild:
        return

    await bot.process_commands(message)

@bot.event
async def on_guild_join(guild):
    if not settings_lib.getGuildExists(guild):
        settings_lib.initServerSettings(guild)
        data_lib.initServerData(guild)

@bot.event
async def on_guild_leave(guild):
    settings_lib.resetServerSettings(guild)
    data_lib.resetServerData(guild)

bot.run(TOKEN, bot=True, reconnect=True)