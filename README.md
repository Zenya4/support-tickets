## Support Tickets
![Icon](https://imgur.com/XirWpLB.png)
### Info
Support tickets bot by Zenya4. More details can be found at https://zenya4.gitlab.io/support-tickets/
### Usage
1. Update the bot with your own token in TOKEN.txt
2. Update the bot with your own mysql credentials in MYSQL.txt
3. If applicable, modify `self.root_dir` in `/storage/data.py` to the directory you are running the bot from
4. Download the required dependencies via pip
  - discord.py
  - mysql-connector-python==8.0.12
  - requests
  - (There may be others, simply install them manually should there be a missing dependency error)
5. Python 3 is required to run the bot. `python3 bot.py`

### Credits
This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.<br><br>
Copyright © Zenya4 ([Zenya#0093](https://discord.gg/KGuaxpM) on Discord)
