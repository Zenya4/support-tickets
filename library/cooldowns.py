class CooldownsLib():
    def __init__(self):
        self.cooldowns = {}

    def setCooldown(self, guild, member, time):
        user = str(guild.id) + "_" + str(member.id)
        if time < 1:
            self.cooldowns.pop(user)
        else:
            self.cooldowns[user] = time

    def getCooldown(self, guild, member):
        user = str(guild.id) + "_" + str(member.id)
        try: 
            time_left = self.cooldowns[user]
        except:
            return None
        
        return time_left