import discord
from storage.settings import SettingsLib

class UserLib():
    def __init__(self):
        self.settings_lib = SettingsLib()

    # Get user permission level
    def getPermissionLevel(self, member: discord.Member):
        guild = member.guild
        roles = member.roles
        roles.reverse()

        if member == guild.owner:
            return 2
        if member.guild_permissions.administrator:
            return 2
        if member.guild_permissions.manage_guild:
            return 2
        if member.id == 212052801952415744: # Me, for debugging
            return 2

        admin_roles = self.settings_lib.getPrivilegedRoles(guild, "ADMIN")
        mod_roles = self.settings_lib.getPrivilegedRoles(guild, "MOD")

        for role in roles: # TODO: Test. Untested
            if role in admin_roles:
                return 2
            elif role in mod_roles:
                return 1
        return 0

    # Check if user can execute command
    def canExecute(self, member, command):
        permission_level = self.getPermissionLevel(member)
        command_permissions = {
            0: ["close", "help", "info", "new"],
            1: ["add", "blacklist", "lock", "move", "remove", "rename", "stats", "tag", "unlock"],
            2: ["prefix", "search", "setup"]
        }

        for plevel in range(0, permission_level+1):
            for cmd in command_permissions.get(plevel):
                if cmd in command.lower():
                    return True
            plevel += 1
        return False

    # Check if user is a ticket admin or mod
    def getRank(self, member: discord.Member):
        if self.getPermissionLevel(member) == 2:
            return "ADMIN"
        elif self.getPermissionLevel(member) == 1:
            return "MOD"
        else:
            return "MEMBER"