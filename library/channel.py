import discord
from library.messages import MessagesLib
from library.overwrites import OverwritesLib
from storage.settings import SettingsLib
from storage.tickets import TicketsLib

class ChannelLib():
    def __init__(self, bot):
        self.messages_lib = MessagesLib(bot)
        self.overwrites_lib = OverwritesLib()
        self.settings_lib = SettingsLib()
        self.tickets_lib = TicketsLib(bot)

    # Check if channel is ticket
    def isTicketChannel(self, text_channel):
        return self.tickets_lib.getTicketExists(text_channel) # Essentially a redirect to avoid breaking API changes

    # Get first matching Category by name
    def getCategory(self, guild, name=""):
        if guild.categories == None:
            return None

        for category in guild.categories:
            if name.upper() == category.name.upper():
                return category

        return None

    # Create a channel for that particular ticket
    async def createTicketChannel(self, msg_object):
        guild = msg_object.guild
        author = msg_object.author

        # Actually create the ticket channel
        ticket_number = str(self.tickets_lib.getGuildTicketCount(guild) + 1)
        ticket_name = "ticket-" + ticket_number
        ticket_overwrites = self.overwrites_lib.getBaseTicketOverwrites(guild, author)
        ticket_category = self.settings_lib.getTicketCategory(guild)
        ticket_reason = self.messages_lib.create_channel(msg_object)

        channel = await guild.create_text_channel(ticket_name, overwrites=ticket_overwrites, category=ticket_category, reason=ticket_reason)
        
        channel_topic = "TICKET-" + str(channel.id)
        await channel.edit(topic=channel_topic, reason=ticket_reason)

        return channel