import discord

class ChecksLib():
    def __init__(self, author, channel):
        self.author = author
        self.channel = channel

    def check_author(self, msg):
        if msg.author is not self.author:
            return False 
        return True

    def check_channel(self, msg):
        if msg.channel is not self.channel:
            return False
        return True

    def check_global(self, msg):
        if self.check_author(msg) and self.check_channel(msg):
            return True
        return False

    def check_roles(self, msg):
        roles = msg.role_mentions
        if roles == None or len(roles) == 0:
            return False
        return True

    def check_int(self, msg, allow_negative=False):
        try:
            integer = int(msg.content)
        except:
            return False

        if int(integer) < 0:
            if allow_negative:
                return True
            return False     
        return True

    def check_category(self, msg):
        if not self.check_int(msg):
            return False
        category_id = int(msg.content)
        category = msg.guild.get_channel(category_id)
        if not isinstance(category, discord.CategoryChannel):
            return False
        return True

    def check_cancel(self, msg):
        if msg.content.lower() == "cancel":
            return False
        return True

    def check_skip(self, msg):
        if msg.content.lower() == "skip":
            return False
        return True