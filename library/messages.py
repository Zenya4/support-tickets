import yaml
import discord
from library.utilities import UtilitiesLib
from library.cooldowns import CooldownsLib
from storage.settings import SettingsLib
from storage.data import DataLib
from storage.tickets import TicketsLib

class MessagesLib():
    def __init__(self, bot):
        self.utilities_lib = UtilitiesLib(bot)
        self.cooldowns_lib = CooldownsLib()
        self.settings_lib = SettingsLib()
        self.data_lib = DataLib()
        self.tickets_lib = TicketsLib(bot)

        self.msg_file = self.data_lib.root_dir + "messages.yml"

        self.load_yaml()

    def load_yaml(self): # TODO: Caching, default file in case parsing fails
        with open(self.msg_file, 'r') as f:
            self.messages = yaml.safe_load(f)

    # Errors
    def no_permission(self, msg_object):
        msg = str(self.messages['errors']['no-permission']['message'])
        clr = str(self.messages['errors']['no-permission']['color'])
        return self._return(msg_object, msg, clr)

    def setup_not_done(self, msg_object):
        msg = str(self.messages['errors']['setup-not-done']['message'])
        clr = str(self.messages['errors']['setup-not-done']['color'])
        return self._return(msg_object, msg, clr)

    def invalid_command(self, msg_object):
        msg = str(self.messages['errors']['invalid-command']['message'])
        clr = str(self.messages['errors']['invalid-command']['color'])
        return self._return(msg_object, msg, clr)

    def invalid_args(self, msg_object):
        msg = str(self.messages['errors']['invalid-args']['message'])
        clr = str(self.messages['errors']['invalid-args']['color'])
        return self._return(msg_object, msg, clr)

    def not_ticket_channel(self, msg_object):
        msg = str(self.messages['errors']['not-ticket-channel']['message'])
        clr = str(self.messages['errors']['not-ticket-channel']['color'])
        return self._return(msg_object, msg, clr)

    def command_under_cooldown(self, msg_object):
        msg = str(self.messages['errors']['command-under-cooldown']['message'])
        clr = str(self.messages['errors']['command-under-cooldown']['color'])
        return self._return(msg_object, msg, clr)

    def user_in_blacklist(self, msg_object):
        msg = str(self.messages['errors']['user-in-blacklist']['message'])
        clr = str(self.messages['errors']['user-in-blacklist']['color'])
        return self._return(msg_object, msg, clr)

    def ticket_max_reached(self, msg_object):
        msg = str(self.messages['errors']['ticket-max-reached']['message'])
        clr = str(self.messages['errors']['ticket-max-reached']['color'])
        return self._return(msg_object, msg, clr)

    # Prefix
    def prefix_set_success(self, msg_object):
        msg = str(self.messages['commands']['prefix']['set-success']['message'])
        clr = str(self.messages['commands']['prefix']['set-success']['color'])
        return self._return(msg_object, msg, clr)

    def prefix_set_fail(self, msg_object):
        msg = str(self.messages['commands']['prefix']['set-fail']['message'])
        clr = str(self.messages['commands']['prefix']['set-fail']['color'])
        return self._return(msg_object, msg, clr)

    # Setup
    def setup_cancelled(self, msg_object):
        msg = str(self.messages['commands']['setup']['cancelled']['message'])
        clr = str(self.messages['commands']['setup']['cancelled']['color'])
        return self._return(msg_object, msg, clr)

    def setup_setting_skipped(self, msg_object):
        msg = str(self.messages['commands']['setup']['setting-skipped']['message'])
        clr = str(self.messages['commands']['setup']['setting-skipped']['color'])
        return self._return(msg_object, msg, clr)

    def setup_done(self, msg_object):
        msg = str(self.messages['commands']['setup']['done']['message'])
        clr = str(self.messages['commands']['setup']['done']['color'])
        return self._return(msg_object, msg, clr)

    def admin_roles_ask(self, msg_object):
        msg = str(self.messages['commands']['setup']['admin-roles']['ask']['message'])
        clr = str(self.messages['commands']['setup']['admin-roles']['ask']['color'])
        return self._return(msg_object, msg, clr)

    def admin_roles_set_success(self, msg_object):
        msg = str(self.messages['commands']['setup']['admin-roles']['set-success']['message'])
        clr = str(self.messages['commands']['setup']['admin-roles']['set-success']['color'])
        return self._return(msg_object, msg, clr)

    def admin_roles_set_fail(self, msg_object):
        msg = str(self.messages['commands']['setup']['admin-roles']['set-fail']['message'])
        clr = str(self.messages['commands']['setup']['admin-roles']['set-fail']['color'])
        return self._return(msg_object, msg, clr)

    def mod_roles_ask(self, msg_object):
        msg = str(self.messages['commands']['setup']['mod-roles']['ask']['message'])
        clr = str(self.messages['commands']['setup']['mod-roles']['ask']['color'])
        return self._return(msg_object, msg, clr)

    def mod_roles_set_success(self, msg_object):
        msg = str(self.messages['commands']['setup']['mod-roles']['set-success']['message'])
        clr = str(self.messages['commands']['setup']['mod-roles']['set-success']['color'])
        return self._return(msg_object, msg, clr)

    def mod_roles_set_fail(self, msg_object):
        msg = str(self.messages['commands']['setup']['mod-roles']['set-fail']['message'])
        clr = str(self.messages['commands']['setup']['mod-roles']['set-fail']['color'])
        return self._return(msg_object, msg, clr)

    def mention_roles_ask(self, msg_object):
        msg = str(self.messages['commands']['setup']['mention-roles']['ask']['message'])
        clr = str(self.messages['commands']['setup']['mention-roles']['ask']['color'])
        return self._return(msg_object, msg, clr)

    def mention_roles_set_success(self, msg_object):
        msg = str(self.messages['commands']['setup']['mention-roles']['set-success']['message'])
        clr = str(self.messages['commands']['setup']['mention-roles']['set-success']['color'])
        return self._return(msg_object, msg, clr)

    def mention_roles_set_fail(self, msg_object):
        msg = str(self.messages['commands']['setup']['mention-roles']['set-fail']['message'])
        clr = str(self.messages['commands']['setup']['mention-roles']['set-fail']['color'])
        return self._return(msg_object, msg, clr)

    def ticket_cooldown_ask(self, msg_object):
        msg = str(self.messages['commands']['setup']['ticket-cooldown']['ask']['message'])
        clr = str(self.messages['commands']['setup']['ticket-cooldown']['ask']['color'])
        return self._return(msg_object, msg, clr)

    def ticket_cooldown_set_success(self, msg_object):
        msg = str(self.messages['commands']['setup']['ticket-cooldown']['set-success']['message'])
        clr = str(self.messages['commands']['setup']['ticket-cooldown']['set-success']['color'])
        return self._return(msg_object, msg, clr)

    def ticket_cooldown_set_fail(self, msg_object):
        msg = str(self.messages['commands']['setup']['ticket-cooldown']['set-fail']['message'])
        clr = str(self.messages['commands']['setup']['ticket-cooldown']['set-fail']['color'])
        return self._return(msg_object, msg, clr)

    def ticket_category_ask(self, msg_object):
        msg = str(self.messages['commands']['setup']['ticket-category']['ask']['message'])
        clr = str(self.messages['commands']['setup']['ticket-category']['ask']['color'])
        return self._return(msg_object, msg, clr)

    def ticket_category_set_success(self, msg_object):
        msg = str(self.messages['commands']['setup']['ticket-category']['set-success']['message'])
        clr = str(self.messages['commands']['setup']['ticket-category']['set-success']['color'])
        return self._return(msg_object, msg, clr)

    def ticket_category_set_fail(self, msg_object):
        msg = str(self.messages['commands']['setup']['ticket-category']['set-fail']['message'])
        clr = str(self.messages['commands']['setup']['ticket-category']['set-fail']['color'])
        return self._return(msg_object, msg, clr)

    def ticket_max_ask(self, msg_object):
        msg = str(self.messages['commands']['setup']['ticket-max']['ask']['message'])
        clr = str(self.messages['commands']['setup']['ticket-max']['ask']['color'])
        return self._return(msg_object, msg, clr)

    def ticket_max_set_success(self, msg_object):
        msg = str(self.messages['commands']['setup']['ticket-max']['set-success']['message'])
        clr = str(self.messages['commands']['setup']['ticket-max']['set-success']['color'])
        return self._return(msg_object, msg, clr)

    def ticket_max_set_fail(self, msg_object):
        msg = str(self.messages['commands']['setup']['ticket-max']['set-fail']['message'])
        clr = str(self.messages['commands']['setup']['ticket-max']['set-fail']['color'])
        return self._return(msg_object, msg, clr)

    def ticket_close_time_ask(self, msg_object):
        msg = str(self.messages['commands']['setup']['ticket-close-time']['ask']['message'])
        clr = str(self.messages['commands']['setup']['ticket-close-time']['ask']['color'])
        return self._return(msg_object, msg, clr)

    def ticket_close_time_set_success(self, msg_object):
        msg = str(self.messages['commands']['setup']['ticket-close-time']['set-success']['message'])
        clr = str(self.messages['commands']['setup']['ticket-close-time']['set-success']['color'])
        return self._return(msg_object, msg, clr)

    def ticket_close_time_set_fail(self, msg_object):
        msg = str(self.messages['commands']['setup']['ticket-close-time']['set-fail']['message'])
        clr = str(self.messages['commands']['setup']['ticket-close-time']['set-fail']['color'])
        return self._return(msg_object, msg, clr)

    def logging_channel_ask(self, msg_object):
        msg = str(self.messages['commands']['setup']['logging-channel']['ask']['message'])
        clr = str(self.messages['commands']['setup']['logging-channel']['ask']['color'])
        return self._return(msg_object, msg, clr)

    def logging_channel_set_success(self, msg_object):
        msg = str(self.messages['commands']['setup']['logging-channel']['set-success']['message'])
        clr = str(self.messages['commands']['setup']['logging-channel']['set-success']['color'])
        return self._return(msg_object, msg, clr)

    def logging_channel_set_fail(self, msg_object):
        msg = str(self.messages['commands']['setup']['logging-channel']['set-fail']['message'])
        clr = str(self.messages['commands']['setup']['logging-channel']['set-fail']['color'])
        return self._return(msg_object, msg, clr)

    # Add
    def add_success(self, msg_object):
        msg = str(self.messages['commands']['add']['success']['message'])
        clr = str(self.messages['commands']['add']['success']['color'])
        return self._return(msg_object, msg, clr)

    def add_fail(self, msg_object):
        msg = str(self.messages['commands']['add']['fail']['message'])
        clr = str(self.messages['commands']['add']['fail']['color'])
        return self._return(msg_object, msg, clr)

    # Blacklist
    def blacklist_add_success(self, msg_object):
        msg = str(self.messages['commands']['blacklist']['add-success']['message'])
        clr = str(self.messages['commands']['blacklist']['add-success']['color'])
        return self._return(msg_object, msg, clr)

    def blacklist_remove_success(self, msg_object):
        msg = str(self.messages['commands']['blacklist']['remove-success']['message'])
        clr = str(self.messages['commands']['blacklist']['remove-success']['color'])
        return self._return(msg_object, msg, clr)

    def blacklist_list_success(self, msg_object):
        msg = str(self.messages['commands']['blacklist']['list-success']['message'])
        clr = str(self.messages['commands']['blacklist']['list-success']['color'])
        return self._return(msg_object, msg, clr)

    def blacklist_fail(self, msg_object):
        msg = str(self.messages['commands']['blacklist']['fail']['message'])
        clr = str(self.messages['commands']['blacklist']['fail']['color'])
        return self._return(msg_object, msg, clr)

    # Lock
    def lock_success(self, msg_object):
        msg = str(self.messages['commands']['lock']['success']['message'])
        clr = str(self.messages['commands']['lock']['success']['color'])
        return self._return(msg_object, msg, clr)

    # Unlock
    def unlock_success(self, msg_object):
        msg = str(self.messages['commands']['unlock']['success']['message'])
        clr = str(self.messages['commands']['unlock']['success']['color'])
        return self._return(msg_object, msg, clr)

    # Remove
    def remove_success(self, msg_object):
        msg = str(self.messages['commands']['remove']['success']['message'])
        clr = str(self.messages['commands']['remove']['success']['color'])
        return self._return(msg_object, msg, clr)

    def remove_fail(self, msg_object):
        msg = str(self.messages['commands']['remove']['fail']['message'])
        clr = str(self.messages['commands']['remove']['fail']['color'])
        return self._return(msg_object, msg, clr)

    # Rename
    def rename_success(self, msg_object):
        msg = str(self.messages['commands']['rename']['success']['message'])
        clr = str(self.messages['commands']['rename']['success']['color'])
        return self._return(msg_object, msg, clr)

    def rename_fail(self, msg_object):
        msg = str(self.messages['commands']['rename']['fail']['message'])
        clr = str(self.messages['commands']['rename']['fail']['color'])
        return self._return(msg_object, msg, clr)

    # Tag
    def tag_add_success(self, msg_object):
        msg = str(self.messages['commands']['tag']['add-success']['message'])
        clr = str(self.messages['commands']['tag']['add-success']['color'])
        return self._return(msg_object, msg, clr)

    def tag_remove_success(self, msg_object):
        msg = str(self.messages['commands']['tag']['remove-success']['message'])
        clr = str(self.messages['commands']['tag']['remove-success']['color'])
        return self._return(msg_object, msg, clr)

    def tag_list_success(self, msg_object):
        msg = str(self.messages['commands']['tag']['list-success']['message'])
        clr = str(self.messages['commands']['tag']['list-success']['color'])
        return self._return(msg_object, msg, clr)

    def tag_fail(self, msg_object):
        msg = str(self.messages['commands']['tag']['fail']['message'])
        clr = str(self.messages['commands']['tag']['fail']['color'])
        return self._return(msg_object, msg, clr)

    # New
    def new_title(self, msg_object):
        msg = str(self.messages['commands']['new']['title']['message'])
        clr = str(self.messages['commands']['new']['title']['color'])
        return self._return(msg_object, msg, clr)
    
    def new_success(self, msg_object):
        msg = str(self.messages['commands']['new']['success']['message'])
        clr = str(self.messages['commands']['new']['success']['color'])
        return self._return(msg_object, msg, clr)

    # Close
    def close_warning(self, msg_object):
        msg = str(self.messages['commands']['close']['warning']['message'])
        clr = str(self.messages['commands']['close']['warning']['color'])
        return self._return(msg_object, msg, clr)
    
    def close_abort(self, msg_object):
        msg = str(self.messages['commands']['close']['abort']['message'])
        clr = str(self.messages['commands']['close']['abort']['color'])
        return self._return(msg_object, msg, clr)

    # Help
    def help(self, msg_object, param: str):
        param = param.lower()
        msg = str(self.messages['commands']['help'][param]['message'])
        clr = str(self.messages['commands']['help'][param]['color'])
        return self._return(msg_object, msg, clr)

    # Move
    def move_success(self, msg_object):
        msg = str(self.messages['commands']['move']['success']['message'])
        clr = str(self.messages['commands']['move']['success']['color'])
        return self._return(msg_object, msg, clr)
    
    def move_fail(self, msg_object):
        msg = str(self.messages['commands']['move']['fail']['message'])
        clr = str(self.messages['commands']['move']['fail']['color'])
        return self._return(msg_object, msg, clr)

    # Audit log
    def modify_channel_permissions(self, msg_object):
        msg = str(self.messages['audit']['modify-channel-permissions']['message'])
        return self._return(msg_object, msg, clr="", return_type="string")

    def modify_channel_name(self, msg_object):
        msg = str(self.messages['audit']['modify-channel-name']['message'])
        return self._return(msg_object, msg, clr="", return_type="string")

    def create_channel(self, msg_object):
        msg = str(self.messages['audit']['create-channel']['message'])
        return self._return(msg_object, msg, clr="", return_type="string")

    def delete_channel(self, msg_object):
        msg = str(self.messages['audit']['delete-channel']['message'])
        return self._return(msg_object, msg, clr="", return_type="string")

    def move_channel(self, msg_object):
        msg = str(self.messages['audit']['move-channel']['message'])
        return self._return(msg_object, msg, clr="", return_type="string")

    # Logging
    def logging_add(self, msg_object):
        msg = str(self.messages['logging']['add']['message'])
        clr = str(self.messages['logging']['add']['color'])
        return self._return(msg_object, msg, clr)

    def logging_remove(self, msg_object):
        msg = str(self.messages['logging']['remove']['message'])
        clr = str(self.messages['logging']['remove']['color'])
        return self._return(msg_object, msg, clr)

    def logging_lock(self, msg_object):
        msg = str(self.messages['logging']['lock']['message'])
        clr = str(self.messages['logging']['lock']['color'])
        return self._return(msg_object, msg, clr)

    def logging_unlock(self, msg_object):
        msg = str(self.messages['logging']['unlock']['message'])
        clr = str(self.messages['logging']['unlock']['color'])
        return self._return(msg_object, msg, clr)

    def logging_rename(self, msg_object):
        msg = str(self.messages['logging']['rename']['message'])
        clr = str(self.messages['logging']['rename']['color'])
        return self._return(msg_object, msg, clr)

    def logging_tag_add(self, msg_object):
        msg = str(self.messages['logging']['tag-add']['message'])
        clr = str(self.messages['logging']['tag-add']['color'])
        return self._return(msg_object, msg, clr)

    def logging_tag_remove(self, msg_object):
        msg = str(self.messages['logging']['tag-remove']['message'])
        clr = str(self.messages['logging']['tag-remove']['color'])
        return self._return(msg_object, msg, clr)

    def logging_blacklist_add(self, msg_object):
        msg = str(self.messages['logging']['blacklist-add']['message'])
        clr = str(self.messages['logging']['blacklist-add']['color'])
        return self._return(msg_object, msg, clr)

    def logging_blacklist_remove(self, msg_object):
        msg = str(self.messages['logging']['blacklist-remove']['message'])
        clr = str(self.messages['logging']['blacklist-remove']['color'])
        return self._return(msg_object, msg, clr)
    
    def logging_new(self, msg_object):
        msg = str(self.messages['logging']['new']['message'])
        clr = str(self.messages['logging']['new']['color'])
        return self._return(msg_object, msg, clr)

    def logging_close(self, msg_object):
        msg = str(self.messages['logging']['close']['message'])
        clr = str(self.messages['logging']['close']['color'])
        return self._return(msg_object, msg, clr)

    def logging_move(self, msg_object):
        msg = str(self.messages['logging']['move']['message'])
        clr = str(self.messages['logging']['move']['color'])
        return self._return(msg_object, msg, clr)

    def _return(self, msg_object, msg, clr="", return_type="embed"):
        guild = msg_object.guild
        channel = msg_object.channel
        channelname = channel.name
        author = msg_object.author
        mentions = msg_object.mentions
        channel_mentions = msg_object.channel_mentions
        content = msg_object.content

        # @mentions
        str_mentions = ""
        if not mentions == None and not len(mentions) == 0:
            for userrole in mentions:
                str_mentions += userrole.mention
        if not content == None and not len(content) == 0:
            for check_user in content.split(' '):
                user = guild.get_member_named(check_user)
                if not user == None and user.mention not in str_mentions:
                    str_mentions += user.mention

        # @channels
        str_channel_mentions = ""
        if not channel_mentions == None and not len(channel_mentions) == 0:
            for _channel in channel_mentions:
                str_channel_mentions += _channel.mention
        else:
            str_channel_mentions = channel.mention

        # @blacklist
        str_blacklist_mentions = "None"
        blacklist = self.data_lib.getBlacklistedMembers(guild)
        if not len(blacklist) == 0:
            str_blacklist_mentions = ""
            for member in blacklist:
                str_blacklist_mentions += "%s\n" % (member.mention)

        # @tags
        str_tags = "None"
        tags = self.tickets_lib.getTags(channel)
        if not len(tags) == 0:
            str_tags = ""
            for tag in tags:
                str_tags += "`%s`\n" % (tag)

        # @timeout
        str_timeout = str(self.settings_lib.getTicketCloseTime(guild))

        # @ticket
        str_ticket = ""
        ticket_count = self.tickets_lib.getGuildTicketCount(guild)
        ticket_name = "ticket-" + str(ticket_count)
        
        for channel in guild.channels:
            if(channel.name) == ticket_name:
                str_ticket = channel.mention

        # Start replacing
        msg = msg.replace("@username", author.name)
        msg = msg.replace("@sender", author.mention)
        msg = msg.replace("@content", content)
        msg = msg.replace("@command", content.split(' ', 1)[0])
        
        try:
            msg = msg.replace("@args2", content.split(' ', 2)[2])
        except IndexError:
            msg = msg.replace("@args2", "None")
        try:
            msg = msg.replace("@args", content.split(' ', 1)[1])
        except IndexError:
            msg = msg.replace("@args", "None")

        msg = msg.replace("@prefix", self.data_lib.getPrefix(guild))
        msg = msg.replace("@mentions", str_mentions)
        msg = msg.replace("@channels", str_channel_mentions)
        msg = msg.replace("@channel", channelname)
        msg = msg.replace("@blacklist", str_blacklist_mentions)
        msg = msg.replace("@tags", str_tags)
        msg = msg.replace("@timeout", str_timeout)
        msg = msg.replace("@ticket", str_ticket)

        if return_type == "string":
            return msg

        else:
            dclr = self.utilities_lib.convertColor(clr)
            embed = self.utilities_lib.embedMessage(author=author, content=msg, color=dclr)
            return embed


