import typing
import discord
from storage.settings import SettingsLib

class OverwritesLib():
    def __init__(self):
        self.settings_lib = SettingsLib()

    def getBaseTicketOverwrites(self, guild, author): # Used when creating a new ticket

        admin_roles = self.settings_lib.getPrivilegedRoles(guild, "ADMIN")
        mod_roles = self.settings_lib.getPrivilegedRoles(guild, "MOD")

        overwrites = {
            guild.me: discord.PermissionOverwrite(read_messages=True, send_messages=True),
            guild.default_role: discord.PermissionOverwrite(read_messages=False, send_messages=False),
            author: discord.PermissionOverwrite(read_messages=True, send_messages=True)
        }

        for admin_role in admin_roles:
            overwrites[admin_role] = discord.PermissionOverwrite(read_messages=True, send_messages=True)

        for mod_role in mod_roles:
            overwrites[mod_role] = discord.PermissionOverwrite(read_messages=True, send_messages=True)

        return overwrites

    async def setBulkOverwrites(self, channel: discord.TextChannel, author: discord.Member, target: list, read=None, write=None):
        parsed_targets = []
        
        for item in target:
            if isinstance(item, discord.Member) or isinstance(item, discord.Role):
                parsed_targets.append(item)

        if len(parsed_targets) == 0:
            return False

        if read == None and write == None:
            overwrite = discord.PermissionOverwrite()
        elif read == None:
            overwrite = discord.PermissionOverwrite(send_messages=write)
        elif write == None:
            overwrite = discord.PermissionOverwrite(read_messages=read)
        else:
            overwrite = discord.PermissionOverwrite(read_messages=read, send_messages=write)

        for parsed_target in parsed_targets:
            await channel.set_permissions(parsed_target, overwrite=overwrite, reason="Channel permissions modified by %s" % (author.mention))

        return True