import random
import requests
import discord
from storage.settings import SettingsLib
from storage.data import DataLib

class UtilitiesLib():
    def __init__(self, bot):
        self.bot = bot
        self.settings_lib = SettingsLib()
        self.data_lib = DataLib()

        self.files_dir = self.data_lib.root_dir + "files/"

    # Check if the role is a privileged one
    def isPrivilegedRole(self, guild, check_role: discord.Role, priv_type):
        priv_role_ids = []
        priv_roles = self.settings_lib.getPrivilegedRoles(guild, priv_type)

        if priv_roles == None or len(priv_roles) == 0:
            return False

        for priv_role in priv_roles:
            priv_role_ids.append(priv_role.id)

        if check_role.id in priv_role_ids:
            return True
        
        return False

    # Embed message
    def embedMessage(self, author: discord.Member, content: str, attachments: discord.Attachment = None, color="", include_author=False, include_role=False):
        if not isinstance(color, discord.Color):
            color = self.randomColor()

        avatar = author.avatar_url
        file = None
        embed = discord.Embed(description=content, colour=color)

        if include_author:
            embed.set_author(name=author, icon_url=avatar)

        if include_role:
            roles = author.roles
            top_role = str(roles[-1])
            embed.set_author(name=author + ' - ' + top_role, icon_url=avatar)
        
        embed.set_footer(text="Support Tickets | By Zenya#0093")
        
        if not attachments == None and not len(attachments) == 0:
            for attachment in attachments:
                url = attachment.url
                if url.find('/'):
                    filepath = self.files_dir # TODO: Add autocleaner for directory
                    filename = url.rsplit('/', 1)[1]
                r = requests.get(url, allow_redirects=True)
                open(filepath + filename, 'wb').write(r.content)
                file = discord.File(filepath + filename, filename=filename)

                if attachment.height != None and attachment.width != None:
                    embed.set_image(url='attachment://' + filename)
                else:
                    embed.add_field(name='Files', value='attachment://' + filename, inline=False)

        if file == None:
            return embed
        else:
            return (embed, file)

    # Log message in logging channel
    async def log(self, guild: discord.Guild, log_embed: discord.Embed):
        channel = self.settings_lib.getLoggingChannel(guild)

        if(channel == None):
            return

        await channel.send(embed=log_embed)

    # Convert color into discord.Color()
    def convertColor(self, clr):
        clr = str(clr.lower())

        switcher = {
            "default": discord.Color.default(),
            "teal": discord.Color.teal(),
            "dark_teal": discord.Color.dark_teal(),
            "green": discord.Color.green(),
            "dark_green": discord.Color.dark_green(),
            "blue": discord.Color.blue(),
            "dark_blue": discord.Color.dark_blue(),
            "purple": discord.Color.purple(),
            "dark_purple": discord.Color.dark_purple(),
            "magenta": discord.Color.magenta(),
            "dark_magenta": discord.Color.dark_magenta(),
            "gold": discord.Color.gold(),
            "dark_gold": discord.Color.dark_gold(),
            "orange": discord.Color.orange(),
            "dark_orange": discord.Color.dark_orange(),
            "red": discord.Color.red(),
            "dark_red": discord.Color.dark_red(),
            "lighter_grey": discord.Color.lighter_grey(),
            "dark_grey": discord.Color.dark_grey(),
            "light_grey": discord.Color.light_grey(),
            "darker_grey": discord.Color.darker_grey(),
            "blurple": discord.Color.blurple(),
            "greyple": discord.Color.greyple(),
        }

        dclr = switcher.get(clr, switcher['default'])
        return dclr

    # Random color selector
    def randomColor(self, args="rand"):
        colors = [
            discord.Color.teal(),
            discord.Color.dark_teal(),
            discord.Color.green(),
            discord.Color.dark_green(),
            discord.Color.blue(),
            discord.Color.dark_blue(),
            discord.Color.purple(),
            discord.Color.dark_purple(),
            discord.Color.magenta(),
            discord.Color.dark_magenta(),
            discord.Color.gold(),
            discord.Color.dark_gold(),
            discord.Color.orange(),
            discord.Color.dark_orange(),
            discord.Color.red(),
            discord.Color.dark_red()
        ]

        if args.contains("rand"):
            pass
        elif args.contains("ext"):
            colors_extended = [
                discord.Color.default(),
                discord.Color.lighter_grey(),
                discord.Color.dark_grey(),
                discord.Color.light_grey(),
                discord.Color.darker_grey(),
                discord.Color.blurple(),
                discord.Color.greyple()
            ]

            colors.extend(colors_extended)
        
        n = random.randint(0, len(colors)-1)
        color = colors[n]
        return(color)