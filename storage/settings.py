import os
import json
import ast
import discord
import mysql.connector
from storage.data import DataLib

class SettingsLib(): # TODO: Null checks for everything since things can be changed
    def __init__(self):
        data_lib = DataLib()

        with open(data_lib.mysql_file, 'r') as f:
            contents = f.read()
            self.dbconfig = {
                "host": contents.splitlines()[0],
                "database": contents.splitlines()[1],
                "user": contents.splitlines()[2],
                "password": contents.splitlines()[3]
            }
            f.close()

    # Initialise bot settings configuration
    def initBotSettings(self):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="settingspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("CREATE TABLE IF NOT EXISTS settings(id INT AUTO_INCREMENT PRIMARY KEY)")
        mysql_cmd.execute("ALTER TABLE settings ADD COLUMN guild BIGINT NOT NULL")
        mysql_cmd.execute("ALTER TABLE settings ADD COLUMN admin_roles JSON")
        mysql_cmd.execute("ALTER TABLE settings ADD COLUMN mod_roles JSON")
        mysql_cmd.execute("ALTER TABLE settings ADD COLUMN mention_roles JSON")
        mysql_cmd.execute("ALTER TABLE settings ADD COLUMN ticket_cooldown INT")
        mysql_cmd.execute("ALTER TABLE settings ADD COLUMN ticket_category BIGINT")
        mysql_cmd.execute("ALTER TABLE settings ADD COLUMN ticket_max INT")
        mysql_cmd.execute("ALTER TABLE settings ADD COLUMN ticket_close_time INT")
        mysql_cmd.execute("ALTER TABLE settings ADD COLUMN logging_channel BIGINT")
        mysql_cmd.execute("ALTER TABLE settings ADD COLUMN setup_done TINYINT NOT NULL DEFAULT 0")
        mysql_db.commit()
        mysql_db.close()

    # Initialise server settings configuration
    def initServerSettings(self, guild, admin_roles="{}", mod_roles="{}", mention_roles="{}", ticket_cooldown=60, ticket_category=0, ticket_max=3, ticket_close_time=10, logging_channel=0):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="settingspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()
        
        mysql_cmd.execute("INSERT INTO settings(guild, admin_roles, mod_roles, mention_roles, ticket_cooldown, ticket_category, ticket_max, ticket_close_time, logging_channel) VALUES(%d, '%s', '%s', '%s', %d, %d, %d, %d, %d)" % (guild.id, admin_roles, mod_roles, mention_roles, ticket_cooldown, ticket_category, ticket_max, ticket_close_time, logging_channel))
        mysql_db.commit()
        mysql_db.close()

    # Reset server settings configuration
    def resetServerSettings(self, guild):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="settingspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("UPDATE settings SET admin_roles = '{}' WHERE guild = '%d'" % (guild.id))
        mysql_cmd.execute("UPDATE settings SET mod_roles = '{}' WHERE guild = '%d'" % (guild.id))
        mysql_cmd.execute("UPDATE settings SET mention_roles = '{}' WHERE guild = '%d'" % (guild.id))
        mysql_cmd.execute("UPDATE settings SET ticket_cooldown = 60 WHERE guild = '%d'" % (guild.id))
        mysql_cmd.execute("UPDATE settings SET ticket_category = 0 WHERE guild = '%d'" % (guild.id))
        mysql_cmd.execute("UPDATE settings SET ticket_max = 3 WHERE guild = '%d'" % (guild.id))
        mysql_cmd.execute("UPDATE settings SET ticket_close_time = 10 WHERE guild = '%d'" % (guild.id))
        mysql_cmd.execute("UPDATE settings SET logging_channel = 0 WHERE guild = '%d'" % (guild.id))
        mysql_cmd.execute("UPDATE settings SET setup_done = 0 WHERE guild = '%d'" % (guild.id))
        mysql_db.commit()
        mysql_db.close()

    # Get if Guild exists in DB
    def getGuildExists(self, guild):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="settingspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT guild FROM settings WHERE guild = '%d'" % (guild.id))
        response = mysql_cmd.fetchone()
        mysql_db.commit()
        mysql_db.close()

        if not response:
            return False
        return True

    # Get Moderator / Admin Roles from DB
    def getPrivilegedRoles(self, guild, priv_type):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="settingspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        priv_type = str(priv_type).upper()
        roles = []

        if priv_type == "ADMIN" or priv_type == "ADMINISTRATOR":
            mysql_cmd.execute("SELECT admin_roles FROM settings WHERE guild = '%d'" % (guild.id))
        elif priv_type == "MOD" or priv_type == "MODERATOR":
            mysql_cmd.execute("SELECT mod_roles FROM settings WHERE guild = '%d'" % (guild.id))
        elif priv_type == "MENTION" or priv_type == "MENTIONED":
            mysql_cmd.execute("SELECT mention_roles FROM settings WHERE guild = '%d'" % (guild.id))
        else:
            return []

        role_ids = mysql_cmd.fetchone()[0].decode()
        mysql_db.commit()
        mysql_db.close()

        role_ids = ast.literal_eval(role_ids)
        
        for role_id in role_ids:
            role = guild.get_role(role_id) # Getting Role from ID

            if role is not None: # If the Role exists on the guild
                roles.append(role) # Add to the list of privileged roles to return

        return roles

    # Get ticket-creation cooldown from configuration
    def getTicketCooldown(self, guild):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="settingspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT ticket_cooldown FROM settings WHERE guild = '%d'" % (guild.id))
        ticket_cooldown = mysql_cmd.fetchone()[0]
        mysql_db.commit()
        mysql_db.close()

        return ticket_cooldown

    # Get ticket-creation Category ID from configuration and return the actual Category
    def getTicketCategory(self, guild):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="settingspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT ticket_category FROM settings WHERE guild = '%d'" % (guild.id))
        cat_id = mysql_cmd.fetchone()[0]
        mysql_db.commit()
        mysql_db.close()

        cat = guild.get_channel(cat_id)

        return cat

    # Get maximum number of concurrent tickets per Member from configuration
    def getTicketMax(self, guild):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="settingspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT ticket_max FROM settings WHERE guild = '%d'" % (guild.id))
        max_tickets = mysql_cmd.fetchone()[0]
        mysql_db.commit()
        mysql_db.close()

        return max_tickets

    # Get reaction time before a ticket is actually closed after executing -close
    def getTicketCloseTime(self, guild):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="settingspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT ticket_close_time FROM settings WHERE guild = '%d'" % (guild.id))
        close_time = mysql_cmd.fetchone()[0]
        mysql_db.commit()
        mysql_db.close()

        return close_time

    # Get log-channel ID from configuration and return the actual Channel
    def getLoggingChannel(self, guild):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="settingspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT logging_channel FROM settings WHERE guild = '%d'" % (guild.id))
        channel_id = mysql_cmd.fetchone()[0]
        mysql_db.commit()
        mysql_db.close()

        channel = guild.get_channel(channel_id)

        return channel

    # Check if setup is done from configuration and returns a boolean
    def getSetupDone(self, guild):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="settingspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT setup_done FROM settings WHERE guild = '%d'" % (guild.id))
        setup_done = mysql_cmd.fetchone()[0]
        mysql_db.commit()
        mysql_db.close()

        if setup_done == 1:
            return True
        return False

    # Set Moderator / Admin Roles in DB
    def setPrivilegedRoles(self, guild, priv_type, priv_roles):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="settingspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        if priv_roles == None or len(priv_roles) == 0:
            return False

        priv_type = str(priv_type).upper()
        priv_role_ids = []

        for role in priv_roles:
            priv_role_ids.append(role.id)

        json_priv_role_ids = json.dumps(priv_role_ids)

        if priv_type == "ADMIN" or priv_type == "ADMINISTRATOR":
            col = "admin_roles"
        elif priv_type == "MOD" or priv_type == "MODERATOR":
            col = "mod_roles"
        elif priv_type == "MENTION" or priv_type == "MENTIONED":
            col = "mention_roles"
        else:
            return False

        mysql_cmd.execute("UPDATE settings SET %s = '%s' WHERE guild = '%d'" % (col, json_priv_role_ids, guild.id))
        mysql_db.commit()
        mysql_db.close()
        return True

    # Set ticket-creation cooldown in configuration
    def setTicketCooldown(self, guild, cooldown):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="settingspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        if not isinstance(cooldown, int):
            return False

        mysql_cmd.execute("UPDATE settings SET ticket_cooldown = '%d' WHERE guild = '%d'" % (cooldown, guild.id))
        mysql_db.commit()
        mysql_db.close()
        return True

    # Set ticket-creation Category ID in configuration from the actual Category
    def setTicketCategory(self, guild, category):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="settingspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        if not isinstance(category, discord.CategoryChannel):
            return False

        mysql_cmd.execute("UPDATE settings SET ticket_category = '%d' WHERE guild = '%d'" % (category.id, guild.id))
        mysql_db.commit()
        mysql_db.close()
        return True

    # Set ticket-creation cooldown in configuration
    def setTicketMax(self, guild, max_tickets):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="settingspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        if not isinstance(max_tickets, int):
            return False

        mysql_cmd.execute("UPDATE settings SET ticket_max = '%d' WHERE guild = '%d'" % (max_tickets, guild.id))
        mysql_db.commit()
        mysql_db.close()
        return True

    # Set reaction time before a ticket is actually closed after executing -close
    def setTicketCloseTime(self, guild, close_time):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="settingspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        if not isinstance(close_time, int):
            return False

        mysql_cmd.execute("UPDATE settings SET ticket_close_time = '%d' WHERE guild = '%d'" % (close_time, guild.id))
        mysql_db.commit()
        mysql_db.close()
        return True

    # Set logging Channel ID in configuration from the actual Channel
    def setLoggingChannel(self, guild, channel):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="settingspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        if not isinstance(channel, discord.TextChannel):
            return False

        mysql_cmd.execute("UPDATE settings SET logging_channel = '%d' WHERE guild = '%d'" % (channel.id, guild.id))
        mysql_db.commit()
        mysql_db.close()
        return True

    def setSetupDone(self, guild, setup_done=True):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="settingspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        if not isinstance(setup_done, bool):
            return False

        if setup_done:
            done_int = 1
        else:
            done_int = 0

        mysql_cmd.execute("UPDATE settings SET setup_done = '%d' WHERE guild = '%d'" % (done_int, guild.id))
        mysql_db.commit()
        mysql_db.close()
        return True

    

        