import os
import json
import ast
import discord
import mysql.connector
from mysql.connector import pooling

class DataLib(): # TODO: Null checks for everything since things can be changed
    def __init__(self):
        self.root_dir = ""
        self.token_file = self.root_dir + "TOKEN.txt"
        self.mysql_file = self.root_dir + "MYSQL.txt"

        with open(self.mysql_file, 'r') as f:
            contents = f.read()
            self.dbconfig = {
                "host": contents.splitlines()[0],
                "database": contents.splitlines()[1],
                "user": contents.splitlines()[2],
                "password": contents.splitlines()[3]
            }
            f.close()

    # Initialise bot data configuration
    def initBotData(self):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="datapool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("CREATE TABLE IF NOT EXISTS data(id INT AUTO_INCREMENT PRIMARY KEY)")
        mysql_cmd.execute("ALTER TABLE data ADD COLUMN guild BIGINT NOT NULL")
        mysql_cmd.execute("ALTER TABLE data ADD COLUMN prefix TEXT NOT NULL")
        mysql_cmd.execute("ALTER TABLE data ADD COLUMN blacklist JSON")
        mysql_db.commit()
        mysql_db.close()

    # Initialise server data configuration
    def initServerData(self, guild, prefix="-", blacklist="{}"):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="datapool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("INSERT INTO data(guild, prefix, blacklist) VALUES(%d, '%s', '%s')" % (guild.id, prefix, blacklist))
        mysql_db.commit()
        mysql_db.close()

    # Reset server data configuration
    def resetServerData(self, guild):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="datapool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("UPDATE data SET prefix = '-' WHERE guild = '%d'" % (guild.id))
        mysql_cmd.execute("UPDATE data SET blacklist = '{}' WHERE guild = '%d'" % (guild.id))
        mysql_db.commit()
        mysql_db.close()

    # Get if Guild exists in DB
    def getGuildExists(self, guild):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="datapool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT guild FROM data WHERE guild = '%d'" % (guild.id))
        response = mysql_cmd.fetchone()
        mysql_db.commit()
        mysql_db.close()

        if not response:
            return False
        return True

    # Get Guild prefix from configuration and return an array
    def getPrefix(self, guild):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="datapool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT prefix FROM data WHERE guild = '%d'" % (guild.id))
        prefix = mysql_cmd.fetchone()[0]
        mysql_db.commit()
        mysql_db.close()

        return prefix

    # Get a list of blacklisted User IDs from configuration and return an actual list of Members
    def getBlacklistedMembers(self, guild):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="datapool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        members = []

        mysql_cmd.execute("SELECT blacklist FROM data WHERE guild = '%d'" % (guild.id))
        
        member_ids = mysql_cmd.fetchone()[0].decode()
        mysql_db.commit()
        mysql_db.close()

        if str(member_ids) == "{}": # MySQL data empty
            return members # Empty list

        member_ids = ast.literal_eval(member_ids)
        
        for member_id in member_ids:
            member = guild.get_member(member_id) # Getting Member from ID

            if not member == None: # If the Member exists on the guild
                members.append(member) # Add to the list of blacklisted members to return

        return members

    # Set guild prefix to configuration from array
    def setGuildPrefix(self, guild, prefix):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="datapool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        if not isinstance(prefix, str):
            return False

        mysql_cmd.execute("UPDATE data SET prefix = '%s' WHERE guild = '%d'" % (prefix, guild.id))
        mysql_db.commit()
        mysql_db.close()
        return True

    # Modify blacklisted User IDs in configuration from the actual Member
    def modBlacklistedMember(self, guild, member, action):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="datapool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        if not isinstance(member, discord.Member):
            return False

        action = str(action).upper()
        bl_member_ids = []

        bl_members = self.getBlacklistedMembers(guild)

        if action == "ADD" or action == "APPEND":
            bl_members.append(member)
        elif action == "REMOVE" or action == "DELETE":
            if not member in bl_members:
                return False
            bl_members.remove(member)
        else:
            return False

        for bl_member in bl_members:
            bl_member_ids.append(bl_member.id)

        json_bl_member_ids = json.dumps(bl_member_ids)

        mysql_cmd.execute("UPDATE data SET blacklist = '%s' WHERE guild = '%d'" % (json_bl_member_ids, guild.id))
        mysql_db.commit()
        mysql_db.close()
        return True