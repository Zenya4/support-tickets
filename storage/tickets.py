import os
import json
import ast
import discord
import mysql.connector
from storage.data import DataLib

class TicketsLib(): # TODO: Null checks for everything since things can be changed
    def __init__(self, bot):
        data_lib = DataLib()
        self.bot = bot

        with open(data_lib.mysql_file, 'r') as f:
            contents = f.read()
            self.dbconfig = {
                "host": contents.splitlines()[0],
                "database": contents.splitlines()[1],
                "user": contents.splitlines()[2],
                "password": contents.splitlines()[3]
            }
            f.close()

    # Initialise bot data configuration
    def initBotTickets(self):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("CREATE TABLE IF NOT EXISTS tickets(id INT AUTO_INCREMENT PRIMARY KEY)")
        mysql_cmd.execute("ALTER TABLE tickets ADD COLUMN channel BIGINT NOT NULL")
        mysql_cmd.execute("ALTER TABLE tickets ADD COLUMN name TEXT NOT NULL")
        mysql_cmd.execute("ALTER TABLE tickets ADD COLUMN guild BIGINT NOT NULL")
        mysql_cmd.execute("ALTER TABLE tickets ADD COLUMN tags JSON")
        mysql_cmd.execute("ALTER TABLE tickets ADD COLUMN created_by BIGINT NOT NULL")
        mysql_cmd.execute("ALTER TABLE tickets ADD COLUMN closed_by BIGINT")
        mysql_cmd.execute("ALTER TABLE tickets ADD COLUMN created_on DATE NOT NULL")
        mysql_cmd.execute("ALTER TABLE tickets ADD COLUMN closed_on DATE")
        mysql_cmd.execute("ALTER TABLE tickets ADD COLUMN is_open TINYINT NOT NULL DEFAULT 1")
        mysql_cmd.execute("ALTER TABLE tickets ADD COLUMN transcript TEXT")
        mysql_db.commit()
        mysql_db.close()

    # Reset server ticket data
    def resetServerTicket(self, guild):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("DELETE FROM tickets WHERE guild = '%d'" % (guild.id))
        mysql_db.commit()
        mysql_db.close()

    # Create a ticket row
    def createTicket(self, channel, name, guild, creator, date, tags="{}"):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("INSERT INTO tickets(channel, name, guild, created_by, created_on, tags) VALUES(%d, '%s', %d, %d, '%s', '%s')" % (channel.id, name, guild.id, creator.id, date, tags))
        mysql_db.commit()
        mysql_db.close()

    # Get if ticket exists in DB
    def getTicketExists(self, channel):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT channel FROM tickets WHERE channel = '%d'" % (channel.id))
        response = mysql_cmd.fetchone()
        mysql_db.commit()
        mysql_db.close()

        if not response:
            return False
        return True

    # Get ticket Guild in DB
    def getGuild(self, channel):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT guild FROM tickets WHERE channel = '%d'" % (channel.id))
        guild_id = mysql_cmd.fetchone()[0]
        mysql_db.commit()
        mysql_db.close()

        guild = self.bot.get_guild(guild_id)

        return guild

    # Get ticket name in DB
    def getName(self, channel):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT name FROM tickets WHERE channel = '%d'" % (channel.id))
        name = mysql_cmd.fetchone()[0]
        mysql_db.commit()
        mysql_db.close()

        return name

    # Get ticket tags in DB and return a List
    def getTags(self, channel):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT tags FROM tickets WHERE channel = '%d'" % (channel.id))
        tags = mysql_cmd.fetchone()
        mysql_db.commit()
        mysql_db.close()

        # In case method is called by @tags
        try:
            tags = tags[0].decode()
        except:
            tags = None

        if tags == None or str(tags) == "{}": # MySQL data empty
            tags = "[]"

        tags = ast.literal_eval(tags)

        return tags
    
    # Get ticket creator in DB
    def getCreator(self, channel):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT created_by FROM tickets WHERE channel = '%d'" % (channel.id))
        creator_id = mysql_cmd.fetchone()[0]
        mysql_db.commit()
        mysql_db.close()

        creator = self.getGuild(channel.id).get_member(creator_id)

        return creator
    
    # Get ticket resolver in DB
    def getResolver(self, channel):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT closed_by FROM tickets WHERE channel = '%d'" % (channel.id))
        resolver_id = mysql_cmd.fetchone()[0]
        mysql_db.commit()
        mysql_db.close()

        resolver = self.getGuild(channel.id).get_member(resolver_id)

        return resolver
    
    # Get ticket creation date in DB
    def getCreationDate(self, channel):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT created_on FROM tickets WHERE channel = '%d'" % (channel.id))
        creation_date = mysql_cmd.fetchone()[0]
        mysql_db.commit()
        mysql_db.close()

        return creation_date
    
    # Get ticket resolution date in DB
    def getResolutionDate(self, channel):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT closed_on FROM tickets WHERE channel = '%d'" % (channel.id))
        resolution_date = mysql_cmd.fetchone()[0]
        mysql_db.commit()
        mysql_db.close()

        return resolution_date

    # Get if ticket is open in DB
    def getTicketOpen(self, channel):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT is_open FROM tickets WHERE channel = '%d'" % (channel.id))
        is_open = int(mysql_cmd.fetchone()[0])
        mysql_db.commit()
        mysql_db.close()

        if is_open == 1:
            return True
        return False

    # Get per-guild ticket count from DB
    def getGuildTicketCount(self, guild, open_only=False):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        if open_only: # Select open tickets only
            mysql_cmd.execute("SELECT channel FROM tickets WHERE guild = '%d' AND is_open = 1" % (guild.id))
        else: # Select all tickets
            mysql_cmd.execute("SELECT channel FROM tickets WHERE guild = '%d'" % (guild.id))

        mysql_cmd.fetchall()
        mysql_db.commit()
        mysql_db.close()

        rowcount = mysql_cmd.rowcount 
        return rowcount

    # Get per-user ticket count in guild from DB
    def getUserTicketCount(self, guild, user, open_only=True):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        if open_only: # Select open tickets only
            mysql_cmd.execute("SELECT channel FROM tickets WHERE guild = '%d' AND created_by = '%d' AND is_open = 1" % (guild.id, user.id))
        else: # Select all tickets
            mysql_cmd.execute("SELECT channel FROM tickets WHERE guild = '%d' AND created_by = '%d'" % (guild.id, user.id))

        mysql_cmd.fetchall()
        mysql_db.commit()
        mysql_db.close()

        rowcount = mysql_cmd.rowcount 
        return rowcount

    # Get top ticket creation/closure statistics
    def getTopData(self, guild, column, count):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT %s, count(%s) FROM tickets WHERE guild = '%d' GROUP BY %s ORDER BY count(%s) DESC" % (column, column, guild.id, column, column))
        try:
            top_data = mysql_cmd.fetchall()[count-1]
        except:
            top_data = ["None", 0]
        mysql_db.commit()
        mysql_db.close()

        return top_data

    # Get ticket transcript (message log html file)
    def getTranscript(self, channel):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("SELECT transcript FROM tickets WHERE channel = '%d'" % (channel.id))
        transcript = str(mysql_cmd.fetchone()[0])
        mysql_db.commit()
        mysql_db.close()

        return str(transcript)

    # Set ticket name in DB
    def setName(self, channel, name):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("UPDATE tickets SET name = '%s' WHERE channel = '%d'" % (name, channel.id))
        mysql_db.commit()
        mysql_db.close()
        return True

    # Modify ticket tags in DB
    def modTicketTags(self, channel, tag, action):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        action = str(action).upper()
        tag = str(tag).upper()

        if tag == "":
            return False

        tags = self.getTags(channel)

        if action == "ADD" or action == "APPEND":
            tags.append(tag)
        elif action == "REMOVE" or action == "DELETE":
            if not tag in tags:
                return False
            tags.remove(tag)
        else:
            return False

        json_tags = json.dumps(tags)

        mysql_cmd.execute("UPDATE tickets SET tags = '%s' WHERE channel = '%d'" % (json_tags, channel.id))
        mysql_db.commit()
        mysql_db.close()
        return True

    # Set ticket resolver in DB
    def setResolver(self, channel, resolver):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        if not isinstance(resolver, discord.Member):
            return False

        mysql_cmd.execute("UPDATE tickets SET closed_by = '%d' WHERE channel = '%d'" % (resolver.id, channel.id))
        mysql_db.commit()
        mysql_db.close()
        return True

    # Set ticket resolution date in DB
    def setResolutionDate(self, channel, date):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        mysql_cmd.execute("UPDATE tickets SET closed_on = '%s' WHERE channel = '%d'" % (date, channel.id))
        mysql_db.commit()
        mysql_db.close()
        return True

    # Set ticket open status in DB
    def setTicketOpen(self, channel, open):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()

        if not isinstance(open, bool):
            return False

        if open:
            open_int = 1
        else:
            open_int = 0

        mysql_cmd.execute("UPDATE tickets SET is_open = '%d' WHERE channel = '%d'" % (open_int, channel.id))
        mysql_db.commit()
        mysql_db.close()
        return True

    # Set ticket transcript (message log html file)
    def setTranscript(self, channel, transcript):
        cnxpool = mysql.connector.pooling.MySQLConnectionPool(pool_name="ticketspool", pool_size=32, pool_reset_session=True, **self.dbconfig)
        mysql_db = cnxpool.get_connection()
        mysql_cmd = mysql_db.cursor()
        
        mysql_cmd.execute("UPDATE tickets SET transcript = '%s' WHERE channel = '%d'" % (transcript, channel.id))
        mysql_db.commit()
        mysql_db.close()

        return True